﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
using System.Configuration;
using System.Web.Security;
using System.Security.Cryptography;

namespace Audit
{
    public partial class GridDinamic2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                BindData();

            }

        }

        private void BindData()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ToString());

            SqlDataAdapter dAd = new SqlDataAdapter("select * from Procurement_B", conn);

            DataSet dSet = new DataSet();

            try
            {

                dAd.Fill(dSet, "PagesData");

                GridView1.DataSource = dSet.Tables["PagesData"].DefaultView;

                GridView1.DataBind();

            }

            catch (Exception ee)
            {

                lblMessage.Text = ee.Message.ToString();

            }

            finally
            {

                dSet.Dispose();

                dAd.Dispose();

                conn.Close();

                conn.Dispose();

            }

        }

        protected void EditRecord(object sender, GridViewEditEventArgs e)
        {

            GridView1.EditIndex = e.NewEditIndex;

            BindData();

        }

        protected void CancelRecord(object sender, GridViewCancelEditEventArgs e)
        {

            GridView1.EditIndex = -1;

            BindData();

        }

        protected void UpdateRecord(object sender, GridViewUpdateEventArgs e)
        {

            GridViewRow row = (GridViewRow)GridView1.Rows[e.RowIndex];

            int autoid = Int32.Parse(GridView1.DataKeys[e.RowIndex].Value.ToString());

            TextBox tPageName = (TextBox)row.FindControl("txtPageName");

            TextBox tPageDesc = (TextBox)row.FindControl("txtPageDesc");

            TextBox tPageName2 = (TextBox)row.FindControl("txtPageDesc2");

            TextBox tPageDesc2 = (TextBox)row.FindControl("txtPageDesc3");

            TextBox tPageDesc3 = (TextBox)row.FindControl("txtPageDesc4");

            //DropDownList dActive = (DropDownList)row.FindControl("dropActive");


            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ToString());

            SqlCommand dCmd = new SqlCommand();

            try
            {

                conn.Open();

                dCmd.CommandText = "spUpdateData";

                dCmd.CommandType = CommandType.StoredProcedure;

                dCmd.Parameters.Add("@AutoID", SqlDbType.Int).Value = autoid;

                dCmd.Parameters.Add("@B_AssetLainnya_Inv", SqlDbType.NVarChar, 50).Value = tPageName.Text.Trim();

                dCmd.Parameters.Add("@B_AssetLainnya_Data", SqlDbType.NVarChar, 50).Value = tPageDesc.Text.Trim();

                dCmd.Parameters.Add("@B_AssetLainnya_Actual", SqlDbType.NVarChar, 50).Value = tPageName2.Text.Trim();

                dCmd.Parameters.Add("@B_AssetLainnya_Diff", SqlDbType.NVarChar, 50).Value = tPageDesc2.Text.Trim();

                dCmd.Parameters.Add("@B_AssetLainnya_Remarks", SqlDbType.NVarChar).Value = tPageDesc3.Text.Trim();

                //dCmd.Parameters.Add("@Active", SqlDbType.Bit).Value = bool.Parse(dActive.SelectedValue);

                dCmd.Connection = conn;

                dCmd.ExecuteNonQuery();


                lblMessage.Text = "Record Updated successfully.";


                GridView1.EditIndex = -1;

                BindData();

            }

            catch (SqlException ee)
            {

                lblMessage.Text = ee.Message;

            }

            finally
            {

                dCmd.Dispose();

                conn.Close();

                conn.Dispose();

            }



        }

        protected void DeleteRecord(object sender, GridViewDeleteEventArgs e)
        {

            string autoid = GridView1.DataKeys[e.RowIndex].Value.ToString();


            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ToString());

            SqlCommand dCmd = new SqlCommand();

            try
            {

                conn.Open();

                dCmd.CommandText = "spDeleteData";

                dCmd.CommandType = CommandType.StoredProcedure;

                dCmd.Parameters.Add("@AutoID", SqlDbType.Int).Value = Int32.Parse(autoid);

                dCmd.Connection = conn;

                dCmd.ExecuteNonQuery();



                lblMessage.Text = "Record Deleted successfully.";



                // Refresh the data

                BindData();



            }

            catch (SqlException ee)
            {

                lblMessage.Text = ee.Message;

            }

            finally
            {

                dCmd.Dispose();

                conn.Close();

                conn.Dispose();

            }

        }

    }
}