﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Audit.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<%--    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>--%>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <script>
        $(function () {
            var dateFormat = "mm/dd/yy",
              from = $("[id$=AuditPeriodeStart]")
                .datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 2
                })
                .on("change", function () {
                    to.datepicker("option", "minDate", getDate(this));
                }),
              to = $("[id$=AuditPeriodeEnd]").datepicker({
                  defaultDate: "+1w",
                  changeMonth: true,
                  changeYear: true,
                  numberOfMonths: 2
              })
              .on("change", function () {
                  from.datepicker("option", "maxDate", getDate(this));
              });

            function getDate(element) {
                var date;
                try {
                    date = $.datepicker.parseDate(dateFormat, element.value);
                } catch (error) {
                    date = null;
                }
                return date;
            }
        });
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">

    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 10pt;
        }
    </style>

    <div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

        <fieldset>
        <legend>Internal Audit Control</legend>
        <h4> Internal Audit Control </h4>

        <ol>
            <li>
                <asp:Label ID="Label6" runat="server" AssociatedControlID="UserName">User name :</asp:Label>
                <asp:TextBox runat="server" ID="UserName" ReadOnly="True"  />
<%--                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="UserName"
                    CssClass="field-validation-error" ErrorMessage="The user name field is required." />--%>
            </li>
            <li>                
                <label for="textarea">BranchID :</label>
                <asp:DropDownList runat="server" ID="ddlBranchID" Width="307px" Height="30px" AutoPostBack="true">
                <asp:ListItem Text = "--Select BranchID--" Value = ""></asp:ListItem>
                </asp:DropDownList>
<%--                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="BranchID"
                    CssClass="field-validation-error" ErrorMessage="The BranchID field is required." />--%>
            </li>
<%--            <li>
                <label for="textarea">BranchName :</label>
                <asp:DropDownList ID="ddlBranchName" runat="server" Width="307px" Height="30px" AutoPostBack="true" >
                <asp:ListItem Text = "--Select BranchName--" Value = ""></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="BranchName"
                    CssClass="field-validation-error" ErrorMessage="The BranchName field is required." />
            </li>--%>
            <li>
                <label for="textarea">Month Of Audit :</label>
                <asp:DropDownList ID="ddlMonth" runat="server" Width="116px" Height="30px">
                    <asp:ListItem Text="--Select Month--" Value=""></asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="ddlYear" runat="server" Width="114px" Height="30px">
                    <asp:ListItem Text="--Select Year--" Value=""></asp:ListItem>
                </asp:DropDownList>
<%--                <label for="textarea">Month Of Audit</label>                
                <asp:TextBox runat="server" ID="txtDate" />--%>
<%--                <asp:RequiredFieldValidator runat="server" ID="reqMOA" ControlToValidate="txtDate" ErrorMessage="Masukan Tanggal dengan format yyyy-mm-dd" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                    ErrorMessage="" ControlToValidate="txtDate" ForeColor="Red" EnableViewState="false"
                    ValidationExpression="\d{4}(\/|\-)((10|11|12)|0[1-9]{1})(\/|\-)(([0-2][1-9])|(3[01]{1}))" Display="Static" SetFocusOnError="False" Visible="True"></asp:RegularExpressionValidator>--%>
            </li>
            <li>
                <asp:Label ID="Label10" runat="server" AssociatedControlID="AuditSchedule">AuditSchedule :</asp:Label>
                <asp:TextBox runat="server" ID="AuditSchedule"/>
<%--                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="AuditSchedule"
                    CssClass="field-validation-error" ErrorMessage="The confirm AuditSchedule field is required." />--%>
            </li>
            <li>
                <asp:Label ID="Label11" runat="server" AssociatedControlID="AuditPeriodeStart">Audit Periode :</asp:Label>
                <asp:TextBox runat="server" ID="AuditPeriodeStart" AutoCompleteType="Disabled" />
                TO                
                <asp:TextBox runat="server" ID="AuditPeriodeEnd" AutoCompleteType="Disabled" />
<%--                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="AuditPeriode"
                    CssClass="field-validation-error" ErrorMessage="The confirm AuditPeriode field is required." />--%>
            </li>
        </ol>
<%--        <asp:Button ID="btnAuditbranch" runat="server" Text="AuditBranch" OnClick="btnAuditbranch_Click1" />--%>
        <asp:Button ID="btnReport" runat="server" Text="Report" />
        <asp:Button ID="btnSave" runat="server" Text="save" OnClick="btnSave_Click" />
    </fieldset>  

        <div>
        <table>
            <tr>
                <td> <asp:Label ID="lblMessage" runat="server" Font-Bold="true"></asp:Label> </td>             
            </tr>
        </table>    
        </div>

</asp:Content>
