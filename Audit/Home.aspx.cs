﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.IO;
using System.Globalization;
using Audit.DAL2;

namespace Audit
{
    public partial class Home : System.Web.UI.Page
    {
        DaLayer ObjDAL2 = new DaLayer();
        //DataAccessLayer ObjDAl = new DataAccessLayer();
        protected void Page_Load(object sender, EventArgs e)
        {
            UserName.Text = this.Page.User.Identity.Name;
            //if (!this.Page.User.Identity.IsAuthenticated)
            //{
            //    FormsAuthentication.RedirectToLoginPage();
            //}

            if (!IsPostBack)
            {                
                FillMasterBrach();
                FillMasterMonth();
                FillMasterYear();
            }

        }
            
       public void FillMasterBrach()
       {
            ddlBranchID.AppendDataBoundItems = true;
            ddlBranchID.DataSource = ObjDAL2.Get_MasterBranch();
            ddlBranchID.DataTextField = "BranchId_BranchName";
            ddlBranchID.DataValueField = "BranchId_BranchName";
            ddlBranchID.DataBind();
       }

       public void FillMasterMonth()
       {
           ddlMonth.AppendDataBoundItems = true;
           ddlMonth.DataSource = ObjDAL2.Get_MasterMonth();
           ddlMonth.DataTextField = "MonthName";
           ddlMonth.DataValueField = "MonthName";
           ddlMonth.DataBind(); 
       }

       public void FillMasterYear()
       {
           ddlYear.AppendDataBoundItems = true;
           ddlYear.DataSource = ObjDAL2.Get_MasterYear();
           ddlYear.DataTextField = "year";
           ddlYear.DataValueField = "year";
           ddlYear.DataBind();
       }      
        
        
        protected void btnSave_Click(object sender, EventArgs e)
        {
            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("SP_SaveData", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("UserName", SqlDbType.NVarChar).Value = UserName.Text;
                    cmd.Parameters.AddWithValue("Branch_ID", ddlBranchID.SelectedItem.Value);
                    //cmd.Parameters.AddWithValue("Branch_Name", ddlBranchName.SelectedItem.Value);
                    cmd.Parameters.AddWithValue("Month", ddlMonth.SelectedItem.Value);
                    cmd.Parameters.AddWithValue("Year", ddlYear.SelectedItem.Value);
                    cmd.Parameters.Add("Audit_Schedule", SqlDbType.NVarChar).Value = AuditSchedule.Text;
                    cmd.Parameters.Add("Audit_Periode_Start", SqlDbType.NVarChar).Value = AuditPeriodeStart.Text;
                    cmd.Parameters.Add("Audit_Periode_End", SqlDbType.NVarChar).Value = AuditPeriodeEnd.Text;
                    cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DateTime.Now;
                    //cmd.Parameters.Add("ID", SqlDbType.Int).Value = ParameterDirection.Output;
                    con.Open();
                    object o = cmd.ExecuteScalar();
                    if (o != null)
                    {
                        string id = o.ToString();
                        //lblMessage.Text = "Your data is been saved in the database";
                        //lblMessage.ForeColor = System.Drawing.Color.Red;
                    }
                    con.Close();

                    Session["ID_SH"] = o.ToString();
                    //Session["BranchID"] = ddlBranchID.SelectedValue;
                    //Session["BranchName"] = ddlBranchName.SelectedValue;
                    //Session["Month"] = ddlMonth.SelectedValue;
                    //Session["Year"] = ddlYear.SelectedValue;
                    //Session["AuditSche"] = AuditSchedule.Text;
                    //Session["AuditStart"] = AuditPeriodeStart.Text;
                    //Session["AuditEnds"] = AuditPeriodeEnd.Text;
                    Response.Redirect("AuditBranch.aspx?ID=" + o.ToString());

                }
            }
 
        }
   
    }        
}