﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Audit.Login" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <style type="text/css">
            body {
                font-family: Arial;
                font-size: 10pt;
                background: url("Images/internal_audit_pichet_w.jpg") no-repeat ;
                background-size:cover;

            }
        input[type=text], input[type=password]
        {
            width: 200px;
        }   
        table
        {
            background: #F7F7F7;
            border: 1px solid #ccc;
        }
        table th
        {
            background-color: #f00;
            color: #f00;
            font-weight: bold;
        }
        table th, table td
        {
            padding: 5px;
            color: #414751;
        }

        .posisi {
            width:400px;
            margin-left:auto;
            margin-right:auto;
            margin-top:150px;


        }
    </style>

    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link href="Scripts/Bootstrap/bootstrap.min.css" rel="stylesheet" media="screen" />

    <script type="text/javascript">
        $(function () {
            $("#btnShowLogin").click(function () {
                $('#LoginModal').modal('show');
            });
        });
    </script>
</head>

<body>
    <div align="right">
        <input type="button" id="btnShowLogin" class="btn btn-primary" value="Login" />
        <input type="button" id="btnShowForgotPass" class="btn btn-primary" value="Forgot Password" runat="server" onserverclick="btnShowForgotPass_ServerClick" />
    </div>
    <form id="form1" runat="server" >
        <div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="ModalTitle" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;</button>
                        <h4 class="modal-title" id="ModalTitle">Login</h4>
                    </div>
                    <div class="modal-body">
                        <label for="txtUsername">
                            NIK</label>
                        <input type="text" runat="server" id="txtUsername" class="form-control" placeholder="Enter NIK" autocomplete="off" pattern="^[0-9]*$" title="_Only Number Format For NIK_" required="" />
                        <br />
                        <label for="txtPassword">
                            Password</label>
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control" placeholder="Enter Password" required="" />
                        <div class="checkbox">
                            <asp:CheckBox ID="chkRememberMe" Text="Remember Me" runat="server" />
                        </div>
                        <div id="dvMessage" runat="server" visible="false" class="alert alert-danger">
                            <strong>Error!</strong>
                            <asp:Label ID="lblMessage" runat="server"  />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnLogin" Text="Login" runat="server" OnClick="ValidateUser" Class="btn btn-primary"/>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </form>
</body>
</html>
