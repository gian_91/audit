﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProcGrid.aspx.cs" Inherits="Audit.ProcGrid" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<%--    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>--%>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

      <style type="text/css">

          .judulrb {
              padding-left:25px;
          }

          .judul {
              text-align:center;
          }

          .judulJob {
              padding-left:90px;
          }

        .txt {
            padding-left: 5px;
            width: 155px;
        }

        .auto-style3 {
            width: 231px;
            height: 49px;
        }

          td {
            padding: 0em 1em 0em 0em;
          }

        .bordered {
            width: 314px;
            height: 225px;
            padding: 3px;
            float: left;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered2 {
            width: 310px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered3 {
            width: 292px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .Table {
            display: table;
            padding-left: 10px; 
        }

        .TableHR {
            display:table;
            text-align:center;
            position:relative;
            right:310px;
        }

        .Tabletanggal {
            display: table;
            width: 1450px;
            text-align: center;
            position: relative;
            right: 250px;
        }

        .TableHeader {
            display: table;
            padding-left: 300px;
        }

        .Table-tanggalbawah {
            display: table;
            position: relative;
            left: 5px;
        }

        .Table-ttd {
            display: table;
            position: relative;
            right: 230px;
        }

        .TableButton {
            display: table;
            position: static;
            padding-left: 956px;
        }

        .div-tableatas {
            display: table;
            width: auto;
            background-color: #eee;
            border: none;
            border-spacing: 10px; /* cellspacing:poor IE support for  this */
        }

        .div-table-row {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-cell {
            display: table-cell;
            width: 140px;
        }

        .div-table-cell-kotak-kanan {
            display: table-cell;
            width: auto;
            clear: both;
            padding-left: 5px;
        }

        .div-table-cell-kotak-kanan-2 {
            display: table-cell;
            width: auto;
            clear: both;
            padding-left: 13px;
        }

        .div-table-header-kanan {
            width: 420px;
            height: 120px;
            border: 1px solid Blue;
            box-sizing: border-box;
            right: -500px;
            position: relative;
        }

        .div-grup {
            width: auto;
            display: table-column-group;
        }

        .div-tableMid {
            display: table;
            width: auto;
            border: 1px solid #666666;
            border-spacing: 2px; /* cellspacing:poor IE support for  this */
        }

        .div-table-rowMid {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-rowMid-ttd {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-colMid {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 190px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colNomer {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 15px;
            height: 38px;
            background-color: #ebfa25;
        }

        .div-table-colEmployee-Id {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 140px;
            height: 38px;
            background-color: #d36f6f;
        }

        .div-table-colNama {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 250px;
            height: 38px;
            background-color: #d36f6f;
        }

        .div-table-colTTL {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 100px;
            height: 38px;
            background-color: #d36f6f;
        }

        .div-table-colDateIn {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 100px;
            height: 38px;
            background-color: #d36f6f;
        }

        .div-table-colDateOut {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 100px;
            height: 38px;
            background-color: #d36f6f;
        }

        .div-table-colGender {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 80px;
            height: 38px;
            background-color: #d36f6f;
        }

        .div-table-colJob {
            float: left; /* fix for  buggy browsers */
            display: table-column;  
            width: 170px;
            height: 38px;
            background-color: #d36f6f;
        }

        
        .div-table-colEmployee-IdTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 140px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-table-colNamaTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 250px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-table-colTTLTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 100px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-table-colDateInTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 100px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-table-colDateOutTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 100px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-table-colGenderTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 80px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-table-colJobTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;  
            width: 170px;
            height: 33px;
            background-color: whitesmoke;
        }


         .div-table-colRainCoat {
            float: left; /* fix for  buggy browsers */
            display: table-column;  
            width: 69px;
            height: 38px;
            background-color: #ebfa25;
        }

         .div-table-colRainCoatTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 69px;
            height: 33px;
            background-color: whitesmoke;
         }

        .div-table-colMidTextBoxMenu {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 210px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-tableMid-TextArea {
            display: table;
            width: 944px;
            background-color: #eee;
            border: 1px solid #666666;
            border-spacing: 5px; /* cellspacing:poor IE support for  this */
        }

        .div-table-colTanggal {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 127px;
            height: 35px;
            background-color: none;
        }

        .div-table-colTanggal2 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 105px;
            height: 35px;
            background-color: none;
        }

        .div-table-colTanggal3 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 70px;
            height: 35px;
            background-color: none;
        }

        .div-table-colMidSelisih {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 944px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidSelisihtextArea {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 944px;
            height: 185px;
            background-color: whitesmoke;
        }

        .div-table-colTTD {
            float: right;
            display: table-column;
            width: 460px;
            height: 30px;
            background-color: none;
        }

        .textarea2 {
            font-family: 'Times New Roman';
            width: 932px;
            height: 175px;
            font-size: 15px;
        }

        .display-validate {
            clear: both;
            display: block;
            position: relative;
            left: 210px;
            bottom: 30px;
        }

        .div-tableMid {
            display: table;
            width: auto;
            background-color: #eee;
            border: 1px solid #666666;
            border-spacing: 5px; /* cellspacing:poor IE support for  this */
        }

        .div-table-rowMid {
            display: table-row;
            width: auto;
            clear: both;
        }

            @media print {
                #printbtn, [id$=btnSave], [id$=btnExit] {
                    display: none;
                }
            }
</style>

        <script>
            $(function () {
                $("[id$=TxbTnglBwh]").datepicker({
                    dateFormat: 'dd/mm/yy',
                    changeMonth: true,
                    changeYear: true
                });
            });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
       <fieldset>
        <legend></legend>
        <div class="Table">
            <div class="div-table-header-kanan">
                <label align="center">IAC GL - 08</label>
                <hr />
                <div class="div-table-row">
                    <div class="div-table-cell-kotak-kanan">
                        <asp:Label ID="Label3" runat="server" AssociatedControlID="TxbDibuatHead">Dibuat </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan">
                        <asp:TextBox ID="TxbDibuatHead" runat="server" ReadOnly="true" Width="110px"></asp:TextBox>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:Label ID="Label1" runat="server" AssociatedControlID="TxbDireviewHead">Direview </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:TextBox ID="TxbDireviewHead" runat="server" ReadOnly="true" Width="110px"></asp:TextBox>
                    </div>
                </div>
                <div class="div-table-row">
                    <div class="div-table-cell-kotak-kanan">
                        <asp:Label ID="Label4" runat="server" AssociatedControlID="TxbTanggalHeadDibuat">Tanggal </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan">
                        <asp:TextBox ID="TxbTanggalHeadDibuat" runat="server" ReadOnly="true" Width="110px"></asp:TextBox>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:Label ID="Label2" runat="server" AssociatedControlID="TxbTanggalHeadDireview">Tanggal </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:TextBox ID="TxbTanggalHeadDireview" runat="server" ReadOnly="true" Width="110px"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="Table">
                <div class="div-table-row">
                    <div class="div-table-cell">
                        <asp:Label ID="lblCabang" runat="server" AssociatedControlID="txbCabang">Bina Artha Cabang </asp:Label>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txbCabang" runat="server" ReadOnly="true" Width="195px"></asp:TextBox>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txtIDSave" runat="server" ReadOnly="true" Width="195px" Visible="false"></asp:TextBox>
                    </div>
                </div>
                <div class="div-table-row">
                    <div class="div-table-cell">
                        <asp:Label ID="lblPeriode" runat="server" AssociatedControlID="txbPeriodeStart">Periode Audit </asp:Label>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txbPeriodeStart" runat="server" ReadOnly="true" Width="83px"></asp:TextBox>to
                    <asp:TextBox ID="txbPeriodeEnd" runat="server" ReadOnly="true" Width="83px"></asp:TextBox>
                    </div>
                </div>                
            </div >
            <br />
            <div class="TableHeader" align="center">
                <h3>BERITA ACARA PEMERIKSAAAN KELENGKAPAN KARYAWAN</h3>
                <h4>(Internal Audit & Control Department)</h4>
            </div>            
        </div>
        <br />
        <br />
        <div class="TableHR">
               <%-- <div class="div-table-rowMid">--%>
            <asp:GridView ID="grvStudentDetails" runat="server" ShowFooter="True" AutoGenerateColumns="False"
                CellPadding="4" ForeColor="#333333" GridLines="None" OnRowDeleting="grvStudentDetails_RowDeleting"
                Width="90%" >
                <Columns>
                    <asp:BoundField  DataField="RowNumber" HeaderText="No" />
                    <asp:TemplateField HeaderText="EmployeeID" >
                        <ItemTemplate>
                            <asp:TextBox ID="TxbEmployeeID" runat="server" MaxLength="12" Width="110px" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TxbEmployeeID"
                                ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Name" HeaderStyle-CssClass="judul">
                        <ItemTemplate>
                            <asp:TextBox ID="TxbName" runat="server"  Width="250px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TxbName"
                                ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Birth_Date" HeaderStyle-CssClass="judulrb">
                        <ItemTemplate>
                            <asp:TextBox ID="TxbBD" runat="server" Width="100px" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TxbBD"
                                ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date_In" HeaderStyle-CssClass="judulrb">
                        <ItemTemplate>
                            <asp:TextBox ID="TxbDI" runat="server" Width="100px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TxbDI"
                                ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date_Out" HeaderStyle-CssClass="judulrb">
                        <ItemTemplate>
                            <asp:TextBox ID="TxbDO" runat="server" Width="100px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TxbDO"
                                ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </ItemTemplate>
                        <FooterStyle HorizontalAlign="Right" />
                        <FooterTemplate>
                            <asp:Button ID="ButtonAdd" runat="server" Text="Add New Row" OnClick="ButtonAdd_Click" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowDeleteButton="True" />
                </Columns>
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#EFF3FB" />
                <EditRowStyle BackColor="#2461BF" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />                
            </asp:GridView>
             <%--   </div>--%>
<%--            </div>--%>
        </div>
        <br />
        <br />
        <br />
        <div class="Tabletanggal">
            <div class="Table-tanggalbawah">
                <div class="div-table-colTanggal">
                    <label>Dilaksanakan Di</label>
                </div>
                <div class="div-table-colTanggal">
                    <asp:TextBox ID="TxbDilaksanakan" runat="server" Width="110px" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
                <div class="div-table-colTanggal2">
                    <label>Pada Tanggal</label>
                </div>
                <div class="div-table-colTanggal">
                    <asp:TextBox ID="TxbTnglBwh" runat="server" Width="110px"></asp:TextBox>
                </div>
                <div class="div-table-colTanggal3">
                    <label>Pada Jam</label>
                </div>
                <div class="div-table-colTanggal">
                    <asp:TextBox ID="TxbPdJam" runat="server" Width="110px" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>
        </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        <div class="Table">
            <div class="Table-ttd">
                <div class="div-table-rowMid-ttd">
                    <div class="div-table-colTTD" align="center">Diperiksa Oleh,</div>
                    <div class="div-table-colTTD" align="Center">Disaksikan Oleh,</div>
                    <div class="div-table-colTTD" align="Center">Mengetahui,</div>
                </div>
                <br />
                <br />
                <br />
                <br />
                <div class="div-table-rowMid-ttd">
                    <div class="div-table-colTTD" align="center">(...............................)</div>
                    <div class="div-table-colTTD" align="Center">(...............................)</div>
                    <div class="div-table-colTTD" align="Center">(...............................)</div>
                </div>
                <div class="div-table-rowMid-ttd">
                    <div class="div-table-colTTD" align="center">Internal Audit&Control</div>
                    <div class="div-table-colTTD" align="Center">Admin/Kasir</div>
                    <div class="div-table-colTTD" align="Center">Branch Manager</div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <br />
        <div class="Table">
            <div class="TableButton">
                
                <input type="button" value="Print" onclick="window.print()" id="printbtn">
                <%--<asp:Button ID="btnPrint" runat="server" Text="Print" Width="100px" OnClick="btnPrint_Click"/>--%>
                <asp:Button ID="btnExit" runat="server" Text="Exit" Width="60px"  />
                <asp:Button ID="btnSave" runat="server" Text="Save" Width="60px" OnClick="btnSave_Click"  />
                <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
            </div>
        </div>


    </fieldset>



</asp:Content>
