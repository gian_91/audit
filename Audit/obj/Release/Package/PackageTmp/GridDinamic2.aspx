﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GridDinamic2.aspx.cs" Inherits="Audit.GridDinamic2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Label ID="lblMessage" runat="Server" ForeColor="Red"></asp:Label>

            <asp:GridView ID="GridView1" runat="Server" AutoGenerateColumns="False" BorderWidth="1"

                DataKeyNames="ID_Proc_B" AutoGenerateEditButton="True" OnRowEditing="EditRecord"

                OnRowCancelingEdit="CancelRecord" OnRowUpdating="UpdateRecord" CellPadding="4"

                HeaderStyle-HorizontalAlign="left" OnRowDeleting="DeleteRecord" RowStyle-VerticalAlign="Top"

                ForeColor="#333333" GridLines="None" Width="688px">

                <Columns>

<%--                    <asp:BoundField DataField="ID_Proc_B" HeaderText="No" ReadOnly="True" />--%>

                    <asp:TemplateField HeaderText="Field1">

                        <ItemTemplate>

                            <%# Eval("B_AssetLainnya_Inv") %>

                        </ItemTemplate>

                        <EditItemTemplate>

                            <asp:TextBox ID="txtPageName" runat="Server" Text='<%# Eval("B_AssetLainnya_Inv") %>' Columns="10"></asp:TextBox>

                            <asp:RequiredFieldValidator ID="req1" runat="Server" Text="*" ControlToValidate="txtPageName"></asp:RequiredFieldValidator>

                        </EditItemTemplate>

                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Field2">

                        <ItemTemplate>

                            <%# Eval("B_AssetLainnya_Data") %>

                        </ItemTemplate>

                        <EditItemTemplate>

                            <asp:TextBox ID="txtPageDesc" runat="Server" Columns="10" Text='<%# Eval("B_AssetLainnya_Data") %>'></asp:TextBox>

                            <asp:RequiredFieldValidator ID="req2" runat="Server" Text="*" ControlToValidate="txtPageDesc"></asp:RequiredFieldValidator>

                        </EditItemTemplate>

                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Field3">

                        <ItemTemplate>

                            <%# Eval("B_AssetLainnya_Actual") %>
                        </ItemTemplate>

                        <EditItemTemplate>

                            <asp:TextBox ID="txtPageDesc2" runat="Server" Columns="10" Text='<%# Eval("B_AssetLainnya_Actual") %>'></asp:TextBox>

                            <asp:RequiredFieldValidator ID="req3" runat="Server" Text="*" ControlToValidate="txtPageDesc2"></asp:RequiredFieldValidator>

                        </EditItemTemplate>

                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Field4">

                        <ItemTemplate>

                            <%# Eval("B_AssetLainnya_Diff") %>
                        </ItemTemplate>

                        <EditItemTemplate>

                            <asp:TextBox ID="txtPageDesc3" runat="Server" Columns="10" Text='<%# Eval("B_AssetLainnya_Diff") %>'></asp:TextBox>

                            <asp:RequiredFieldValidator ID="req4" runat="Server" Text="*" ControlToValidate="txtPageDesc3"></asp:RequiredFieldValidator>

                        </EditItemTemplate>

                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Field5">

                        <ItemTemplate>
                            <%# Eval("B_AssetLainnya_Remarks") %>
                        </ItemTemplate>

                        <EditItemTemplate>

                            <asp:TextBox ID="txtPageDesc4" runat="Server" Columns="10" Text='<%# Eval("B_AssetLainnya_Remarks") %>'></asp:TextBox>

                            <asp:RequiredFieldValidator ID="req5" runat="Server" Text="*" ControlToValidate="txtPageDesc4"></asp:RequiredFieldValidator>

                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Delete?">

                        <ItemTemplate>

                            <span onclick="return confirm('Are you sure to Delete the record?')">

                                <asp:LinkButton ID="lnkB" runat="Server" Text="Delete" CommandName="Delete"></asp:LinkButton>

                            </span>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>

                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />               
                

                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" VerticalAlign="Top" />

                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />

                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />

                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />

                <AlternatingRowStyle BackColor="White" />

            </asp:GridView>


        <asp:SqlDataSource ID="SqlDataSource1" runat="server"
            ConnectionString="<%$ ConnectionStrings:Default %>"
            SelectCommand="SELECT [ID_LHP_Acquisition_Category], [Category_Name] FROM [Master_LHP_Acquisition_Category]"></asp:SqlDataSource>
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False"
            DataKeyNames="ID_LHP_Acquisition_Category" DataSourceID="SqlDataSource1">
            <Columns>
                <asp:BoundField DataField="ID_LHP_Acquisition_Category" HeaderText="ID_LHP_Acquisition_Category"
                    InsertVisible="False" ReadOnly="True" SortExpression="ID_LHP_Acquisition_Category" />

                <asp:TemplateField HeaderText="Categories">
                    <ItemTemplate>      
                        <asp:DropDownList ID="ddlCategories" AutoPostBack="true"
                            DataTextField="Category_Name" DataValueField="ID_LHP_Acquisition_Category"
                            DataSourceID="SqlDataSource1" runat="server" AppendDataBoundItems="true"                            
                            SelectedValue='<%#Bind("ID_LHP_Acquisition_Category")%>'>
                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                        </asp:DropDownList>                
                    </ItemTemplate>                    
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Products">
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlProducts"
                            DataTextField="TOD_Name" DataValueField="ID_LHP_Acquisition_TOD"
                            DataSourceID="SqlDataSource2" runat="server" />
                        <asp:SqlDataSource runat="server" ID="sqlDataSource2"
                            ConnectionString="<%$ ConnectionStrings:Default %>"
                            SelectCommand="SELECT [ID_LHP_Acquisition_TOD], [TOD_Name], ID_LHP_Acquisition_Category FROM [Master_LHP_Acquisition_TOD]"
                            FilterExpression="ID_LHP_Acquisition_Category = '{0}'">
                            <FilterParameters>
                                <asp:ControlParameter Name="categoryParam" ControlID="ddlCategories"
                                    PropertyName="SelectedValue" />
                            </FilterParameters>
                        </asp:SqlDataSource>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
        </asp:GridView>



</asp:Content>
