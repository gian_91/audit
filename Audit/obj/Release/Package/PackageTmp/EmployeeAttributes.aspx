﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeeAttributes.aspx.cs" Inherits="Audit.EmployeeAttributes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .txt {
            padding-left: 5px;
            width: 155px;
        }

        .auto-style3 {
            width: 231px;
            height: 49px;
        }

        .bordered {
            width: 314px;
            height: 225px;
            padding: 3px;
            float: left;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered2 {
            width: 310px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered3 {
            width: 292px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .Table {
            display: table;
            padding-left: 10px; 
        }

        .TableHR {
            display:table;
            width: 1450px;
            text-align:center;
            position:relative;
            right:240px;
        }

        .Tabletanggal {
            display: table;
            width: 1450px;
            text-align: center;
            position: relative;
            right: 280px;
        }

        .TableHeader {
            display: table;
            padding-left: 300px;
        }

        .Table-tanggalbawah {
            display: table;
            position: relative;
            left: 5px;
        }

        .Table-ttd {
            display: table;
            position: relative;
            right: 230px;
        }

        .TableButton {
            display: table;
            position: static;
            padding-left: 956px;
        }

        .div-tableatas {
            display: table;
            width: auto;
            background-color: #eee;
            border: none;
            border-spacing: 10px; /* cellspacing:poor IE support for  this */
        }

        .div-table-row {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-cell {
            display: table-cell;
            width: 140px;
        }

        .div-table-cell-kotak-kanan {
            display: table-cell;
            width: auto;
            clear: both;
            padding-left: 5px;
        }

        .div-table-cell-kotak-kanan-2 {
            display: table-cell;
            width: auto;
            clear: both;
            padding-left: 13px;
        }

        .div-table-header-kanan {
            width: 420px;
            height: 120px;
            border: 1px solid Blue;
            box-sizing: border-box;
            right: -500px;
            position: relative;
        }

        .div-grup {
            width: auto;
            display: table-column-group;
        }

        .div-tableMid {
            display: table;
            width: auto;
            background-color: #eee;
            border: 1px solid #666666;
            border-spacing: 2px; /* cellspacing:poor IE support for  this */
        }

        .div-table-rowMid {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-rowMid-ttd {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-colMid {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 190px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colNomer {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 30px;
            height: 38px;
            background-color: whitesmoke;
        }

        .div-table-colEmployee-Id {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 140px;
            height: 38px;
            background-color: whitesmoke;
        }

        .div-table-colNama {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 250px;
            height: 38px;
            background-color: whitesmoke;
        }

                .div-table-colTTL {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 100px;
            height: 38px;
            background-color: whitesmoke;
        }

        .div-table-colDateIn {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 100px;
            height: 38px;
            background-color: whitesmoke;
        }

        .div-table-colDateOut {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 100px;
            height: 38px;
            background-color: whitesmoke;
        }

        .div-table-colGender {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 80px;
            height: 38px;
            background-color: whitesmoke;
        }

        .div-table-colJob {
            float: left; /* fix for  buggy browsers */
            display: table-column;  
            width: 183px;
            height: 38px;
            background-color: whitesmoke;
        }

         .div-table-colRainCoat {
            float: left; /* fix for  buggy browsers */
            display: table-column;  
            width: 65px;
            height: 38px;
            background-color: whitesmoke;
        }

        .div-table-colMidTextBoxMenu {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 210px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-tableMid-TextArea {
            display: table;
            width: 944px;
            background-color: #eee;
            border: 1px solid #666666;
            border-spacing: 5px; /* cellspacing:poor IE support for  this */
        }

        .div-table-colTanggal {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 127px;
            height: 35px;
            background-color: none;
        }

        .div-table-colTanggal2 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 105px;
            height: 35px;
            background-color: none;
            padding-left: 100px;
        }

        .div-table-colTanggal3 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 70px;
            height: 35px;
            background-color: none;
        }

        .div-table-colMidSelisih {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 944px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidSelisihtextArea {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 944px;
            height: 185px;
            background-color: whitesmoke;
        }

        .div-table-colTTD {
            float: right;
            display: table-column;
            width: 460px;
            height: 30px;
            background-color: none;
        }

        .textarea2 {
            font-family: 'Times New Roman';
            width: 932px;
            height: 175px;
            font-size: 15px;
        }

        .display-validate {
            clear: both;
            display: block;
            position: relative;
            left: 210px;
            bottom: 30px;
        }

        .div-tableMid {
            display: table;
            width: auto;
            background-color: #eee;
            border: 1px solid #666666;
            border-spacing: 5px; /* cellspacing:poor IE support for  this */
        }

        .div-table-rowMid {
            display: table-row;
            width: auto;
            clear: both;
        }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend></legend>
        <div class="Table">
            <div class="div-table-header-kanan">
                <label align="center">IAC GL - 08</label>
                <hr />
                <div class="div-table-row">
                    <div class="div-table-cell-kotak-kanan">
                        <asp:Label ID="Label3" runat="server" AssociatedControlID="TxbDibuatHead">Dibuat </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan">
                        <asp:TextBox ID="TxbDibuatHead" runat="server" ReadOnly="true" Width="110px"></asp:TextBox>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:Label ID="Label1" runat="server" AssociatedControlID="TxbDireviewHead">Direview </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:TextBox ID="TxbDireviewHead" runat="server" ReadOnly="true" Width="110px"></asp:TextBox>
                    </div>
                </div>
                <div class="div-table-row">
                    <div class="div-table-cell-kotak-kanan">
                        <asp:Label ID="Label4" runat="server" AssociatedControlID="TxbTanggalHeadDibuat">Tanggal </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan">
                        <asp:TextBox ID="TxbTanggalHeadDibuat" runat="server" ReadOnly="true" Width="110px"></asp:TextBox>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:Label ID="Label2" runat="server" AssociatedControlID="TxbTanggalHeadDireview">Tanggal </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:TextBox ID="TxbTanggalHeadDireview" runat="server" ReadOnly="true" Width="110px"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="Table">
                <div class="div-table-row">
                    <div class="div-table-cell">
                        <asp:Label ID="lblCabang" runat="server" AssociatedControlID="txbCabang">Bina Artha Cabang </asp:Label>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txbCabang" runat="server" ReadOnly="true" Width="195px"></asp:TextBox>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txtIDSave" runat="server" ReadOnly="true" Width="195px" Visible="false"></asp:TextBox>
                    </div>
                </div>
                <div class="div-table-row">
                    <div class="div-table-cell">
                        <asp:Label ID="lblPeriode" runat="server" AssociatedControlID="txbPeriodeStart">Periode Audit </asp:Label>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txbPeriodeStart" runat="server" ReadOnly="true" Width="83px"></asp:TextBox>to
                    <asp:TextBox ID="txbPeriodeEnd" runat="server" ReadOnly="true" Width="83px"></asp:TextBox>
                    </div>
                </div>                
            </div >
            <br />
            <div class="TableHeader" align="center">
                <h3>BERITA ACARA PEMERIKSAAAN KELENGKAPAN KARYAWAN</h3>
                <h4>(Internal Audit & Control Department)</h4>
            </div>            
        </div>
        <br />
        <br />
        <div class="TableHR">
            <div class="div-tableMid">
                <div class="div-table-rowMid">
                    <div class="div-table-colNomer" align="center">NO</div>
                    <div class="div-table-colEmployee-Id" align="center">Employee ID</div>
                    <div class="div-table-colNama" align="center">Name</div>
                    <div class="div-table-colTTL" align="center">Birth_Date</div>
                    <div class="div-table-colDateIn" align="center">Date In</div>
                    <div class="div-table-colDateOut" align="center">Date Out</div>
                    <div class="div-table-colGender" align="center">Gender</div>
                    <div class="div-table-colJob" align="center">Job</div>
                    <div class="div-table-colRainCoat" align="center">RainCoat</div>
                    <div class="div-table-colRainCoat" align="center">Company Uniform</div>
                    <div class="div-table-colRainCoat" align="center">Name Card</div>
                    <div class="div-table-colRainCoat" align="center">Name Tag</div>
                    <div class="div-table-colRainCoat" align="center">Insurance</div>
                    <div class="div-table-colRainCoat" align="center">Jamsostek</div>
                    <div class="div-table-colRainCoat" align="center">Mutation Letter</div>
                </div>
            </div>
            <div class="div-tableMid">
                <div class="div-table-rowMid">
                    <div class="div-table-colNomer" align="center">1</div>
                    <div class="div-table-colEmployee-Id" align="center">
                        <asp:TextBox ID="TxbEmpID" runat="server" Width="130px"></asp:TextBox>
                    </div>
                    <div class="div-table-colNama" align="center">
                        <asp:TextBox ID="TxbName" runat="server" Width="240px"></asp:TextBox>
                    </div>
                    <div class="div-table-colTTL" align="center">
                        <asp:TextBox ID="TxbBirthDate" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colDateIn" align="center">
                        <asp:TextBox ID="TxbDateIn" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colDateOut" align="center">
                        <asp:TextBox ID="TxbDateOut" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colGender" align="center">
                        <asp:TextBox ID="TxbGender" runat="server" Width="68px"></asp:TextBox>
                    </div>
                    <div class="div-table-colJob" align="center">
                        <asp:TextBox ID="TxbJob" runat="server" Width="168px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CkbRainCoat" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CkbCompanyUniform" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CkbNameCard" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CkbNameTag" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CkbInsurance" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CkbJamsostek" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CkbML" runat="server" />
                    </div>
                </div>
            </div>
            <div class="div-tableMid">
                <div class="div-table-rowMid">
                    <div class="div-table-colNomer" align="center">2</div>
                    <div class="div-table-colEmployee-Id" align="center">
                        <asp:TextBox ID="TextBox8" runat="server" Width="130px"></asp:TextBox>
                    </div>
                    <div class="div-table-colNama" align="center">
                        <asp:TextBox ID="TextBox9" runat="server" Width="240px"></asp:TextBox>
                    </div>
                    <div class="div-table-colTTL" align="center">
                        <asp:TextBox ID="TextBox10" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colDateIn" align="center">
                        <asp:TextBox ID="TextBox11" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colDateOut" align="center">
                        <asp:TextBox ID="TextBox12" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colGender" align="center">
                        <asp:TextBox ID="TextBox13" runat="server" Width="68px"></asp:TextBox>
                    </div>
                    <div class="div-table-colJob" align="center">
                        <asp:TextBox ID="TextBox14" runat="server" Width="168px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox8" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox9" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox10" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox11" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox12" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox13" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox14" runat="server" />
                    </div>
                </div>
            </div>
            <div class="div-tableMid">
                <div class="div-table-rowMid">
                    <div class="div-table-colNomer" align="center">3</div>
                    <div class="div-table-colEmployee-Id" align="center">
                        <asp:TextBox ID="TextBox15" runat="server" Width="130px"></asp:TextBox>
                    </div>
                    <div class="div-table-colNama" align="center">
                        <asp:TextBox ID="TextBox16" runat="server" Width="240px"></asp:TextBox>
                    </div>
                    <div class="div-table-colTTL" align="center">
                        <asp:TextBox ID="TextBox17" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colDateIn" align="center">
                        <asp:TextBox ID="TextBox18" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colDateOut" align="center">
                        <asp:TextBox ID="TextBox19" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colGender" align="center">
                        <asp:TextBox ID="TextBox20" runat="server" Width="68px"></asp:TextBox>
                    </div>
                    <div class="div-table-colJob" align="center">
                        <asp:TextBox ID="TextBox21" runat="server" Width="168px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox15" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox16" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox17" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox18" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox19" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox20" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox21" runat="server" />
                    </div>
                </div>
            </div>
            <div class="div-tableMid">
                <div class="div-table-rowMid">
                    <div class="div-table-colNomer" align="center">4</div>
                    <div class="div-table-colEmployee-Id" align="center">
                        <asp:TextBox ID="TextBox22" runat="server" Width="130px"></asp:TextBox>
                    </div>
                    <div class="div-table-colNama" align="center">
                        <asp:TextBox ID="TextBox23" runat="server" Width="240px"></asp:TextBox>
                    </div>
                    <div class="div-table-colTTL" align="center">
                        <asp:TextBox ID="TextBox24" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colDateIn" align="center">
                        <asp:TextBox ID="TextBox25" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colDateOut" align="center">
                        <asp:TextBox ID="TextBox26" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colGender" align="center">
                        <asp:TextBox ID="TextBox27" runat="server" Width="68px"></asp:TextBox>
                    </div>
                    <div class="div-table-colJob" align="center">
                        <asp:TextBox ID="TextBox28" runat="server" Width="168px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox22" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox23" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox24" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox25" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox26" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox27" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox28" runat="server" />
                    </div>
                </div>
            </div>
            <div class="div-tableMid">
                <div class="div-table-rowMid">
                    <div class="div-table-colNomer" align="center">5</div>
                    <div class="div-table-colEmployee-Id" align="center">
                        <asp:TextBox ID="TextBox29" runat="server" Width="130px"></asp:TextBox>
                    </div>
                    <div class="div-table-colNama" align="center">
                        <asp:TextBox ID="TextBox30" runat="server" Width="240px"></asp:TextBox>
                    </div>
                    <div class="div-table-colTTL" align="center">
                        <asp:TextBox ID="TextBox31" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colDateIn" align="center">
                        <asp:TextBox ID="TextBox32" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colDateOut" align="center">
                        <asp:TextBox ID="TextBox33" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colGender" align="center">
                        <asp:TextBox ID="TextBox34" runat="server" Width="68px"></asp:TextBox>
                    </div>
                    <div class="div-table-colJob" align="center">
                        <asp:TextBox ID="TextBox35" runat="server" Width="168px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox29" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox30" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox31" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox32" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox33" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox34" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox35" runat="server" />
                    </div>
                </div>
            </div>
            <div class="div-tableMid">
                <div class="div-table-rowMid">
                    <div class="div-table-colNomer" align="center">6</div>
                    <div class="div-table-colEmployee-Id" align="center">
                        <asp:TextBox ID="TextBox36" runat="server" Width="130px"></asp:TextBox>
                    </div>
                    <div class="div-table-colNama" align="center">
                        <asp:TextBox ID="TextBox37" runat="server" Width="240px"></asp:TextBox>
                    </div>
                    <div class="div-table-colTTL" align="center">
                        <asp:TextBox ID="TextBox38" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colDateIn" align="center">
                        <asp:TextBox ID="TextBox39" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colDateOut" align="center">
                        <asp:TextBox ID="TextBox40" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colGender" align="center">
                        <asp:TextBox ID="TextBox41" runat="server" Width="68px"></asp:TextBox>
                    </div>
                    <div class="div-table-colJob" align="center">
                        <asp:TextBox ID="TextBox42" runat="server" Width="168px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox36" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox37" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox38" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox39" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox40" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox41" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox42" runat="server" />
                    </div>
                </div>
            </div>
            <div class="div-tableMid">
                <div class="div-table-rowMid">
                    <div class="div-table-colNomer" align="center">7</div>
                    <div class="div-table-colEmployee-Id" align="center">
                        <asp:TextBox ID="TextBox43" runat="server" Width="130px"></asp:TextBox>
                    </div>
                    <div class="div-table-colNama" align="center">
                        <asp:TextBox ID="TextBox44" runat="server" Width="240px"></asp:TextBox>
                    </div>
                    <div class="div-table-colTTL" align="center">
                        <asp:TextBox ID="TextBox45" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colDateIn" align="center">
                        <asp:TextBox ID="TextBox46" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colDateOut" align="center">
                        <asp:TextBox ID="TextBox47" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colGender" align="center">
                        <asp:TextBox ID="TextBox48" runat="server" Width="68px"></asp:TextBox>
                    </div>
                    <div class="div-table-colJob" align="center">
                        <asp:TextBox ID="TextBox49" runat="server" Width="168px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox43" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox44" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox45" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox46" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox47" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox48" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox49" runat="server" />
                    </div>
                </div>
            </div>
            <div class="div-tableMid">
                <div class="div-table-rowMid">
                    <div class="div-table-colNomer" align="center">8</div>
                    <div class="div-table-colEmployee-Id" align="center">
                        <asp:TextBox ID="TextBox50" runat="server" Width="130px"></asp:TextBox>
                    </div>
                    <div class="div-table-colNama" align="center">
                        <asp:TextBox ID="TextBox51" runat="server" Width="240px"></asp:TextBox>
                    </div>
                    <div class="div-table-colTTL" align="center">
                        <asp:TextBox ID="TextBox52" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colDateIn" align="center">
                        <asp:TextBox ID="TextBox53" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colDateOut" align="center">
                        <asp:TextBox ID="TextBox54" runat="server" Width="87px"></asp:TextBox>
                    </div>
                    <div class="div-table-colGender" align="center">
                        <asp:TextBox ID="TextBox55" runat="server" Width="68px"></asp:TextBox>
                    </div>
                    <div class="div-table-colJob" align="center">
                        <asp:TextBox ID="TextBox56" runat="server" Width="168px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox50" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox51" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox52" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox53" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox54" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox55" runat="server" />
                    </div>
                    <div class="div-table-colRainCoat" align="center">
                        <asp:CheckBox ID="CheckBox56" runat="server" />
                    </div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <br />
        <div class="Tabletanggal">
            <div class="Table-tanggalbawah">
                <div class="div-table-colTanggal">
                    <label>Dilaksanakan Di</label>
                </div>
                <div class="div-table-colTanggal">
                    <asp:TextBox ID="TextBox57" runat="server" Width="210px"></asp:TextBox>
                </div>
                <div class="div-table-colTanggal2">
                    <label>Pada Tanggal</label>
                </div>
                <div class="div-table-colTanggal">
                    <asp:TextBox ID="TextBox58" runat="server" Width="110px"></asp:TextBox>
                </div>
                <div class="div-table-colTanggal3">
                    <label>Pada Jam</label>
                </div>
                <div class="div-table-colTanggal">
                    <asp:TextBox ID="TextBox59" runat="server" Width="110px"></asp:TextBox>
                </div>
            </div>
        </div>
        <br />
        <br />
        <div class="Table">
            <div class="Table-ttd">
                <div class="div-table-rowMid-ttd">
                    <div class="div-table-colTTD" align="center">Diperiksa Oleh,</div>
                    <div class="div-table-colTTD" align="Center">Disaksikan Oleh,</div>
                    <div class="div-table-colTTD" align="Center">Mengetahui,</div>
                </div>
                <br />
                <br />
                <br />
                <br />
                <div class="div-table-rowMid-ttd">
                    <div class="div-table-colTTD" align="center">(...............................)</div>
                    <div class="div-table-colTTD" align="Center">(...............................)</div>
                    <div class="div-table-colTTD" align="Center">(...............................)</div>
                </div>
                <div class="div-table-rowMid-ttd">
                    <div class="div-table-colTTD" align="center">Internal Audit&Control</div>
                    <div class="div-table-colTTD" align="Center">Admin/Kasir</div>
                    <div class="div-table-colTTD" align="Center">Branch Manager</div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <br />
        <div class="Table">
            <div class="TableButton">
                <asp:Button ID="btnSave" runat="server" Text="Save" Width="60px" OnClick="btnSave_Click" />
                <input type="button" value="Print" onclick="window.print()">
                <%--<asp:Button ID="btnPrint" runat="server" Text="Print" Width="100px" OnClick="btnPrint_Click"/>--%>
                <asp:Button ID="btnExit" runat="server" Text="Exit" Width="60px" OnClick="btnExit_Click"  />
                <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
            </div>
        </div>

<%--        <div>
            Hobbies:
<asp:CheckBoxList ID="chkHobbies" runat="server" RepeatDirection="Horizontal" >
</asp:CheckBoxList>
<br />
<asp:Button ID="btnUpdate" runat="server" Text="SaveEmpAtt"  />
        </div>--%>
    </fieldset>
</asp:Content>
