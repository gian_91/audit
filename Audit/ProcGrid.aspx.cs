﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Data;
using System.Web.Services;
using System.Collections.Specialized;
using System.Text;

namespace Audit
{
    public partial class ProcGrid : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.TxbTanggalHeadDibuat.Text = DateTime.Today.ToString("dd/MM/yyyy");
            TxbDibuatHead.Text = this.Page.User.Identity.Name;
            if (Session["BranchID"] != null)
                txbCabang.Text = Session["BranchID"].ToString();
            if (Session["AuditStart"] != null)
                txbPeriodeStart.Text = Session["AuditStart"].ToString();
            if (Session["AuditEnd"] != null)
                txbPeriodeEnd.Text = Session["AuditEnd"].ToString();
            if (Session["ID_SH"] != null)
                txtIDSave.Text = Session["ID_SH"].ToString();
            if (!Page.IsPostBack)
            {
                FirstGridViewRow();
            }

        }

        private void FirstGridViewRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Col1", typeof(string)));
            dt.Columns.Add(new DataColumn("Col2", typeof(string)));
            dt.Columns.Add(new DataColumn("Col3", typeof(string)));
            dt.Columns.Add(new DataColumn("Col4", typeof(string)));
            dt.Columns.Add(new DataColumn("Col5", typeof(string)));
            dr = dt.NewRow();
            dr["RowNumber"] = 1;
            dr["Col1"] = string.Empty;
            dr["Col2"] = string.Empty;
            dr["Col3"] = string.Empty;
            dr["Col4"] = string.Empty;
            dr["Col5"] = string.Empty;
            dt.Rows.Add(dr);

            ViewState["CurrentTable"] = dt;


            grvStudentDetails.DataSource = dt;
            grvStudentDetails.DataBind();


            TextBox txn = (TextBox)grvStudentDetails.Rows[0].Cells[1].FindControl("TxbEmployeeID");
            txn.Focus();
            Button btnAdd = (Button)grvStudentDetails.FooterRow.Cells[5].FindControl("ButtonAdd");
            Page.Form.DefaultFocus = btnAdd.ClientID;

        }

        private void AddNewRow()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        TextBox TxbEmployeeIDs = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[1].FindControl("TxbEmployeeID");
                        TextBox TextName = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[2].FindControl("TxbName");
                        TextBox TextBD = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[3].FindControl("TxbBD");
                        TextBox TextDI = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[4].FindControl("TxbDI");
                        TextBox TextDO = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[5].FindControl("TxbDO");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["RowNumber"] = i + 1;

                        dtCurrentTable.Rows[i - 1]["Col1"] = TxbEmployeeIDs.Text;
                        dtCurrentTable.Rows[i - 1]["Col2"] = TextName.Text;
                        dtCurrentTable.Rows[i - 1]["Col3"] = TextBD.Text;
                        dtCurrentTable.Rows[i - 1]["Col4"] = TextDI.Text;
                        dtCurrentTable.Rows[i - 1]["Col5"] = TextDO.Text;


                        rowIndex++;
                    }
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;

                    grvStudentDetails.DataSource = dtCurrentTable;
                    grvStudentDetails.DataBind();

                    TextBox txn = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[1].FindControl("TxbEmployeeID");
                    txn.Focus();
                    // txn.Focus;
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }
            SetPreviousData();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TextBox TxbEmployeeID = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[1].FindControl("TxbEmployeeID");
                        TextBox TextName = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[2].FindControl("TxbName");
                        TextBox TextBD = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[3].FindControl("TxbBD");
                        TextBox TextDI = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[4].FindControl("TxbDI");
                        TextBox TextDO = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[5].FindControl("TxbDO");

                        //drCurrentRow["RowNumber"] = i + 1;

                        grvStudentDetails.Rows[i].Cells[0].Text = Convert.ToString(i + 1);
                        TxbEmployeeID.Text = dt.Rows[i]["Col1"].ToString();
                        TextName.Text = dt.Rows[i]["Col2"].ToString();
                        TextBD.Text = dt.Rows[i]["Col3"].ToString();
                        TextDI.Text = dt.Rows[i]["Col4"].ToString();
                        TextDO.Text = dt.Rows[i]["Col5"].ToString();


                        rowIndex++;
                    }
                }
            }
        }

        private void SetRowData()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        TextBox TextEmpId = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[1].FindControl("TxbEmployeeID");
                        TextBox TextName = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[2].FindControl("TxbName");
                        TextBox TextBD = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[3].FindControl("TxbBD");
                        TextBox TextDI = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[4].FindControl("TxbDI");
                        TextBox TextDO = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[5].FindControl("TxbDO");

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["RowNumber"] = i + 1;
                        dtCurrentTable.Rows[i - 1]["Col1"] = TextEmpId.Text;
                        dtCurrentTable.Rows[i - 1]["Col2"] = TextName.Text;
                        dtCurrentTable.Rows[i - 1]["Col3"] = TextBD.Text;
                        dtCurrentTable.Rows[i - 1]["Col4"] = TextDI.Text;
                        dtCurrentTable.Rows[i - 1]["Col5"] = TextDO.Text;


                        rowIndex++;
                    }

                    ViewState["CurrentTable"] = dtCurrentTable;
                    //grvStudentDetails.DataSource = dtCurrentTable;
                    //grvStudentDetails.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }
            //SetPreviousData();
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            AddNewRow();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int rowIndex = 0;
            StringCollection sc = new StringCollection();
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values  

                        //txtIDSave.Text = dtCurrentTable.Rows.ToString();
                        TextBox box1 = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[1].FindControl("TxbEmployeeID");
                        TextBox box2 = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[2].FindControl("TxbName");
                        TextBox box3 = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[3].FindControl("TxbBD");
                        TextBox box4 = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[4].FindControl("TxbDI");
                        TextBox box5 = (TextBox)grvStudentDetails.Rows[rowIndex].Cells[5].FindControl("TxbDO");
                        //get the values from TextBox and DropDownList  
                        //then add it to the collections with a comma "," as the delimited values  
                        sc.Add(string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", box1.Text, box2.Text, box3.Text, box4.Text, box5.Text,
                        txtIDSave.Text, TxbDibuatHead.Text, TxbTanggalHeadDibuat.Text));
                        rowIndex++;
                    }
                    //Call the method for executing inserts  
                    InsertRecords(sc);
                }
            }

        }

        private string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
        }

        private void InsertRecords(StringCollection sc)
        {
            StringBuilder sb = new StringBuilder(string.Empty);
            txtIDSave.Text = "10";
            string[] splitItems = null;
            const string sqlStatement = "insert into Procurement_B (IDProc_B,B_AssetLainnya_Inv,B_AssetLainnya_Data,B_AssetLainnya_Actual,B_AssetLainnya_Diff,B_AssetLainnya_Remarks) values";
            foreach (string item in sc)
            {
                if (item.Contains(","))
                {
                    splitItems = item.Split(",".ToCharArray());
                    sb.AppendFormat("{0}('{1}','{2}','{3}','{4}','{5}','{6}'); ", 
                        sqlStatement, txtIDSave.Text, splitItems[0], splitItems[1], splitItems[2], splitItems[3], splitItems[4], splitItems[5]);
                }
            }

            using (SqlConnection connection = new SqlConnection(GetConnectionString()))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(sb.ToString(), connection))
                {
                    cmd.Parameters.AddWithValue("@IDEmpAttr", SqlDbType.Int).Value = txtIDSave.Text;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
            }
            lblMessage.Text = "Records successfully saved!";
        }

        protected void grvStudentDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            SetRowData();
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                int rowIndex = Convert.ToInt32(e.RowIndex);
                if (dt.Rows.Count > 1)
                {
                    dt.Rows.Remove(dt.Rows[rowIndex]);
                    drCurrentRow = dt.NewRow();
                    ViewState["CurrentTable"] = dt;
                    grvStudentDetails.DataSource = dt;
                    grvStudentDetails.DataBind();

                    for (int i = 0; i < grvStudentDetails.Rows.Count - 1; i++)
                    {
                        grvStudentDetails.Rows[i].Cells[0].Text = Convert.ToString(i + 1);
                    }
                    SetPreviousData();
                }
            }
        }
    }
}