﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GridDinamic.aspx.cs" Inherits="Audit.GridDinamic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
        <script>
            function ValidateEmptyValue() {
                var gv = document.getElementById("<%= Gridview1.ClientID %>");
            var tb = gv.getElementsByTagName("input");

            for (var i = 0; i < tb.length; i++) {
                if (tb[i].type == "text") {
                    if (tb[i].value < 1) {
                        alert("Field cannot be blank!");
                        return false;
                    }
                }
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
        <h1>Dynamic GridView Row Creation</h1>
    <div>
        <asp:Label ID="lblMessage" runat="server" ForeColor="Green" />
        <asp:gridview ID="Gridview1"  runat="server"  ShowFooter="true" Visible="true"
                             AutoGenerateColumns="false"
                             OnRowCreated="Gridview1_RowCreated">
            <Columns>
                <asp:BoundField DataField="RowNumber" HeaderText="Row Number" />
                <asp:TemplateField HeaderText="Header 1">
                    <ItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Header 2">
                    <ItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField  HeaderText="Header 3">
                    <ItemTemplate>
                        <asp:DropDownList ID="DropDownList1" runat="server"
                                          AppendDataBoundItems="true">
                             <asp:ListItem Value="-1">Select</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Header 4">
                    <ItemTemplate>
                        <asp:DropDownList ID="DropDownList2" runat="server"
                                          AppendDataBoundItems="true">
                             <asp:ListItem Value="-1">Select</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                    <FooterStyle HorizontalAlign="Right" />
                    <FooterTemplate>
                         <asp:Button ID="ButtonAdd" runat="server" 
                                     Text="Add New Row" 
                                     onclick="ButtonAdd_Click"
                                    OnClientClick="return ValidateEmptyValue();" />
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkDelete" runat="server" 
                                        onclick="LinkDelete_Click">Remove</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:gridview> 
        </div>
    <asp:Button ID="BtnSave" runat="server" Text="Save All" OnClick="BtnSave_Click"  />

    <div>
        <asp:Label ID="lblproduct" runat="server" Text="Upload" ></asp:Label>
        <asp:FileUpload ID="FileUploadProduct" runat="server" />
        <asp:Button ID="btnupload" runat="server" Text="Upload" OnClick="btnupload_Click"  />
        <asp:Label ID="MsgAlert" runat="server"></asp:Label>
    </div>
</asp:Content>
