﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using AjaxControlToolkit;

namespace Audit
{
    /// <summary>
    /// Summary description for ServiceCS1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ServiceCS1 : System.Web.Services.WebService
    {
        [WebMethod]
        public CascadingDropDownNameValue[] GetCountries(string knownCategoryValues)
        {
            string query = "SELECT DISTINCT ID_LHP_Acquisition_Category,Category_Name FROM [Master_LHP_Acquisition_Category]";
            List<CascadingDropDownNameValue> countries = GetData(query);
            return countries.ToArray();
        }

        [WebMethod]
        public CascadingDropDownNameValue[] GetCities(string knownCategoryValues)
        {
            string country = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)["ID_LHP_Acquisition_Category"];
            string query = string.Format("SELECT DISTINCT TOD_Name FROM Master_LHP_Acquisition_TOD WHERE ID_LHP_Acquisition_Category = '{0}'", country);
            List<CascadingDropDownNameValue> cities = GetData(query);
            return cities.ToArray();
        }

        private List<CascadingDropDownNameValue> GetData(string query)
        {
            string conString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlCommand cmd = new SqlCommand(query);
            List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                cmd.Connection = con;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        values.Add(new CascadingDropDownNameValue
                        {
                            name = reader[0].ToString(),
                            value = reader[0].ToString()
                        });
                    }
                    reader.Close();
                    con.Close();
                    return values;
                }
            }
        }
    }
}
