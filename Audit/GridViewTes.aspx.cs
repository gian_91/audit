﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using Audit.DAL2;

namespace Audit
{
    public partial class GridViewTes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Call FillGridView Method
                FillGridView();
            }

        }

        public void FillGridView()
        {
            try
            {
                string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                SqlConnection con = new SqlConnection(cnString);
                GlobalClass.adap = new SqlDataAdapter("  select * from Procurement_B", con);
                SqlCommandBuilder bui = new SqlCommandBuilder(GlobalClass.adap);
                GlobalClass.dt = new DataTable();
                GlobalClass.adap.Fill(GlobalClass.dt);
                GridView1.DataSource = GlobalClass.dt;
                GridView1.DataBind();
            }
            catch
            {
                Response.Write("<script> alert('Connection StringError...') </script>");
            }
        }

        protected void editRecord(object sender, GridViewEditEventArgs e)
        {
            //Image imgEditPhoto = GridView1.Rows[e.NewEditIndex].FindControl("imgPhoto") as Image;
            //GlobalClass.imgEditPath = imgEditPhoto.ImageUrl;
            // Get the current row index for edit record
            GridView1.EditIndex = e.NewEditIndex;
            FillGridView();
        }

        protected void cancelRecord(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex  = -1;
            FillGridView();
        }

        protected void AddNewRecord(object sender, EventArgs e)
        {
            try
            {
                if (GlobalClass.dt.Rows.Count > 0)
                {
                    GridView1.EditIndex = -1;
                    GridView1.ShowFooter = true;
                    FillGridView();
                }
                else
                {
                    GridView1.ShowFooter = true;
                    DataRow dr = GlobalClass.dt.NewRow();
                    dr["B_AssetLainnya_Inv"] = "0";
                    dr["B_AssetLainnya_Data"] = 0;
                    dr["B_AssetLainnya_Actual"] = 0;
                    dr["B_AssetLainnya_Diff"] = "0";
                    dr["B_AssetLainnya_Remarks"] = "0";
                    //dr["photopath"] = "0";
                    GlobalClass.dt.Rows.Add(dr);
                    GridView1.DataSource = GlobalClass.dt;
                    GridView1.DataBind();
                    GridView1.Rows[0].Visible = false;
                }
            }
            catch
            {
                Response.Write("<script> alert('Row not added in DataTable...') </script>");
            }
        }

        protected void AddNewCancel(object sender, EventArgs e)
        {
            GridView1.ShowFooter = false;
            FillGridView();
        }

        protected void InsertNewRecord(object sender, EventArgs e)
        {
            try
            {
                string strName = GlobalClass.dt.Rows[0]["B_AssetLainnya_Inv"].ToString();
                if (strName == "0")
                {
                    GlobalClass.dt.Rows[0].Delete();
                    GlobalClass.adap.Update(GlobalClass.dt);
                }
                TextBox txtName = GridView1.FooterRow.FindControl("txtNewName") as TextBox;
                TextBox txtAge = GridView1.FooterRow.FindControl("txtNewAge") as TextBox;
                TextBox txtSalary = GridView1.FooterRow.FindControl("txtNewSalary") as TextBox;
                TextBox txtCountry = GridView1.FooterRow.FindControl("txtNewCountry") as TextBox;
                TextBox txtCity = GridView1.FooterRow.FindControl("txtNewCity") as TextBox;
                //FileUpload fuPhoto = GridView1.FooterRow.FindControl("fuNewPhoto") as FileUpload;
                Guid FileName = Guid.NewGuid();
                //fuPhoto.SaveAs(Server.MapPath("~/Images/" + FileName + ".png"));
                DataRow dr = GlobalClass.dt.NewRow();
                dr["B_AssetLainnya_Inv"] = txtName.Text.Trim();
                dr["B_AssetLainnya_Data"] = txtAge.Text.Trim();
                dr["B_AssetLainnya_Actual"] = txtSalary.Text.Trim();
                dr["B_AssetLainnya_Diff"] = txtCountry.Text.Trim();
                dr["B_AssetLainnya_Remarks"] = txtCity.Text.Trim();
                //dr["photopath"] = "~/Images/" + FileName + ".png";
                GlobalClass.dt.Rows.Add(dr);
                GlobalClass.adap.Update(GlobalClass.dt);
                GridView1.ShowFooter = false;
                FillGridView();
            }
            catch
            {
                Response.Write("<script> alert('Record not added...') </script>");
            }
        }

        protected void updateRecord(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                TextBox txtName = GridView1.Rows[e.RowIndex].FindControl("txtName") as TextBox;
                TextBox txtAge = GridView1.Rows[e.RowIndex].FindControl("txtAge") as TextBox;
                TextBox txtSalary = GridView1.Rows[e.RowIndex].FindControl("txtSalary") as TextBox;
                TextBox txtCountry = GridView1.Rows[e.RowIndex].FindControl("txtCountry") as TextBox;
                TextBox txtCity = GridView1.Rows[e.RowIndex].FindControl("txtCity") as TextBox;
                //FileUpload fuPhoto = GridView1.Rows[e.RowIndex].FindControl("fuPhoto") as FileUpload;
                Guid FileName = Guid.NewGuid();
                //if (fuPhoto.FileName != "")
                //{
                //    fuPhoto.SaveAs(Server.MapPath("~/Images/" + FileName + ".png"));
                //    GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["photopath"] = "~/Images/" + FileName + ".png";
                //    File.Delete(Server.MapPath(GlobalClass.imgEditPath));
                //}
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["B_AssetLainnya_Inv"] = txtName.Text.Trim();
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["B_AssetLainnya_Data"] = Convert.ToInt32(txtAge.Text.Trim());
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["B_AssetLainnya_Actual"] = Convert.ToInt32(txtSalary.Text.Trim());
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["B_AssetLainnya_Diff"] = txtCountry.Text.Trim();
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["B_AssetLainnya_Remarks"] = txtCity.Text.Trim();
                GlobalClass.adap.Update(GlobalClass.dt);
                GridView1.EditIndex = -1;
                FillGridView();
            }
            catch
            {
                Response.Write("<script> alert('Record updation fail...') </script>");
            }
        }

        protected void RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex].Delete();
                GlobalClass.adap.Update(GlobalClass.dt);
                //Image imgPhoto = GridView1.Rows[e.RowIndex].FindControl("imgPhoto") as Image;
                //File.Delete(Server.MapPath(imgPhoto.ImageUrl));
                FillGridView();
            }
            catch
            {
                Response.Write("<script> alert('Record not deleted...') </script>");
            }
        }  
    
 
    
    
    
        
    }
}