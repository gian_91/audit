﻿using System.Web.Services;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using Audit.DAL2;

namespace Audit
{
    public partial class ManageUser : System.Web.UI.Page
    {
        DataTable dt;
        DaLayer dl = new DaLayer();
        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        public void BindGrid()
        {
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM tlbuser order by [UserId]");
                    SqlConnection con = new SqlConnection(cnString);
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }
            }

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                IEnumerable<DataRow> query = from i in dt.AsEnumerable()
                                             where i.Field<Int32>("No").Equals(code)
                                             select i;
                DataTable detailTable = query.CopyToDataTable<DataRow>();
                DetailsView1.DataSource = detailTable;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("editRecord"))///buat edit
            {
                //Getddl1edit();
                GridViewRow gvrow = GridView1.Rows[index];
                lblCountryCode.Text = HttpUtility.HtmlDecode(gvrow.Cells[3].Text).ToString();
                //ddl1edit.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[5].Text);
                //ddl2edit.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[6].Text);
                //ddl3edit.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[7].Text);
                //ddl4edit.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[8].Text);
                txtRFB.Text = HttpUtility.HtmlDecode(gvrow.Cells[9].Text);
                txtRFM.Text = HttpUtility.HtmlDecode(gvrow.Cells[10].Text);
                lblResult.Visible = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                hfCode.Value = code;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)////buat edit
        {
            string No = txtusername.Text;
            //string category = ddl1edit.SelectedItem.Text;
            //string tod = ddl2edit.SelectedItem.Text;
            //string cf = ddl3edit.SelectedItem.Text;
            //string tabel = ddl4edit.SelectedItem.Text;
            string rfb = txtNIK.Text;
            string rfm = txtEmail.Text;
            string rpass = txtPassword.Text;
            string dt = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            executeUpdate(No, rfb, rfm, dt, rpass);///masuk ke private void update                  
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Records Updated Successfully');");
            sb.Append("$('#editModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
        }

        private void executeUpdate(string No, string rfb, string rfm, string dt, string rpass)///buat edit(klik button update di form edit)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand updateCmd = new SqlCommand("insert into tlbuser (Username,NIK,Email,[Password],CreatedDate) values (@no,@rfb,@rfm,@rpass,@dt)) ", conn);
                //updateCmd.CommandType = CommandType.StoredProcedure;
                //updateCmd.Parameters.AddWithValue("@category", category);
                //updateCmd.Parameters.AddWithValue("@tod", tod);
                //updateCmd.Parameters.AddWithValue("@cf", cf);
                //updateCmd.Parameters.AddWithValue("@tabel", tabel);
                updateCmd.Parameters.AddWithValue("@rfb", rfb);
                updateCmd.Parameters.AddWithValue("@rfm", rfm);
                updateCmd.Parameters.AddWithValue("@dt", dt);
                updateCmd.Parameters.AddWithValue("@No", No);
                updateCmd.Parameters.AddWithValue("@dt", rpass);
                updateCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException me)
            {
                System.Console.Error.Write(me.InnerException.Data);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)///buat add, menegluarkan form add
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);
        }

        protected void btnAddRecord_Click(object sender, EventArgs e)///klik buton add di form add
        {
            {
                try
                {
                    string code = txtusername.Text;
                    string name = txtNIK.Text;
                    string region = txtEmail.Text;
                    string continent = txtPassword.Text;
                    //string dt = DateTime.Now.

                    executeAdd(code, name, continent, region);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);
                }
            }        

        }

        private void executeAdd(string code, string name, string continent, string region)///Adding data
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand addCmd = new SqlCommand("SP_ManageUser", conn);
                addCmd.CommandType = CommandType.StoredProcedure;
                    
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.Parameters.AddWithValue("@name", name);
                addCmd.Parameters.AddWithValue("@region", region);
                addCmd.Parameters.AddWithValue("@continent", dl.Encript(continent.Trim()));
                //addCmd.Parameters.AddWithValue("@dt", dt);

                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string code = hfCode.Value;
            executeDelete(code);
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Record deleted Successfully');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);

        }

        private void executeDelete(string code)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string updatecmd = "delete from LHP_Acquisition where No=@code";
                SqlCommand addCmd = new SqlCommand(updatecmd, conn);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }

        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }

    }
}