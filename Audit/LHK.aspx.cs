﻿using System.Web.Services;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Audit
{
    public partial class LHK : System.Web.UI.Page
    {
        DataTable dt;
        protected void Page_Load(object sender, EventArgs e)
        {
            TxbModul.Text = "LHK";
            txbUsername.Text = this.Page.User.Identity.Name;
            if (!IsPostBack)
            {
                //this.TxbTanggalHeadDibuat.Text = DateTime.Today.ToString("dd/MM/yyyy");
                //TxbDibuatHead.Text = this.Page.User.Identity.Name;
                if (Session["BranchID"] != null)
                    txbCabang.Text = Session["BranchID"].ToString();
                if (Session["AuditStart"] != null)
                    txbPeriodeStart.Text = Session["AuditStart"].ToString();
                if (Session["AuditEnd"] != null)
                    txbPeriodeEnd.Text = Session["AuditEnd"].ToString();

                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
            }

            if (Session["ID_SH"] == null || Session["ID_SH_Temp"] == null)
                Response.Redirect("Login.aspx");

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                BindGrid();
                BindGridColl();

            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                BindGrid();
                BindGridColl();
            }
        }

        [WebMethod]
        public static List<string> GetEmpNames(string empName)
        {
            List<string> Emp = new List<string>();
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            string query = string.Format("select AccountID from RawDataCurrent where AccountID LIKE '{0}%'", empName);
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Emp.Add(reader.GetString(0));
                    }
                }
            }
            return Emp;
        }

        protected void btnCcek_Click(object sender, EventArgs e)
        {
            Get_Rdc();
        }

        private void Get_Rdc()
        {
            string message = string.Empty;
            SqlDataReader reader = null;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            SqlCommand cmd = new SqlCommand("select AccountID,BranchName,ClientName,CenterName,DisbursementDate,DueDays,LoanStatus,SOName,ROName,OpenedDate,LoanOutstandingPrincipal," +
                "DisbursedAmount from RawDataCurrent where AccountID=@AccountID and BranchName=@Cabang ", con);
            cmd.Parameters.AddWithValue("@AccountID", txtAccountId.Text);
            cmd.Parameters.AddWithValue("@Cabang", txbCabang.Text);
            reader = cmd.ExecuteReader();
            reader.Read();
            {
                if (reader.HasRows == true)
                {
                    txtOutstanding.Text = Convert.ToString(reader["LoanOutstandingPrincipal"]);
                    txtTicketSize.Text = Convert.ToString(reader["DisbursedAmount"]);
                    txbID_DocTest.Text = Convert.ToString(reader["AccountID"]);
                    txtNamaMitra.Text = Convert.ToString(reader["ClientName"]);
                    txtCentName.Text = Convert.ToString(reader["CenterName"]);
                    txtDisbDate.Text = Convert.ToString(reader["DisbursementDate"]);
                    txtPAR.Text = Convert.ToString(reader["DueDays"]);
                    txtLoanStat.Text = Convert.ToString(reader["LoanStatus"]);
                    txtNamaSO.Text = Convert.ToString(reader["SOName"]);
                    txtNamaRO.Text = Convert.ToString(reader["ROName"]);
                    txtUkmDate.Text = Convert.ToString(reader["OpenedDate"]);
                }
                else
                {
                    txtOutstanding.Text = string.Empty;
                    txtTicketSize.Text = string.Empty;
                    txbID_DocTest.Text = string.Empty;
                    txbID_RDT.Text = string.Empty;
                    txtNamaMitra.Text = string.Empty;
                    txtCentName.Text = string.Empty;
                    txtDisbDate.Text = string.Empty;
                    txtPAR.Text = string.Empty;
                    txtLoanStat.Text = string.Empty;
                    txtNamaSO.Text = string.Empty;
                    txtNamaRO.Text = string.Empty;
                    txtUkmDate.Text = string.Empty;
                    message = "Data tidak Ditemukan di cabang " + txbCabang.Text;
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);

                }
                con.Close();
            }


        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("AuditBranch.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("AuditBranch.aspx?ID=" + txtIDSaveTemp.Text);
            }
        }

        public void BindGrid()
        {
            string IDProc_B = txtIDSaveTemp.Text;
            string IDProc_A = txtIDSave.Text;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                try
                {                    
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_Acquisition where ID_LHK={0}", IDProc_A);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    //string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    //SqlConnection conn = new SqlConnection(connString);
                    con.Open();
                    //string cmd = "SELECT  * FROM LHK_Acquisition where ID_LHK = @ID_LHK ";
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }
 
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_Acquisition where ID_LHK={0}", IDProc_B);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    //string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    //SqlConnection conn = new SqlConnection(connString);
                    con.Open();
                    //string cmd = "SELECT  * FROM LHK_Acquisition where ID_LHK = @ID_LHK ";
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                } 
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                IEnumerable<DataRow> query = from i in dt.AsEnumerable()
                                             where i.Field<Int32>("No").Equals(code)
                                             select i;
                DataTable detailTable = query.CopyToDataTable<DataRow>();
                DetailsView1.DataSource = detailTable;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("editRecord"))///buat edit
            {
                Getddl1edit();
                GridViewRow gvrow = GridView1.Rows[index];
                lblCountryCode.Text = HttpUtility.HtmlDecode(gvrow.Cells[3].Text).ToString();
                ddl1edit.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[5].Text);
                ddl2edit.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[6].Text);
                ddl3edit.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[7].Text);
                ddl4edit.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[8].Text);
                txtRFB.Text = HttpUtility.HtmlDecode(gvrow.Cells[9].Text);
                txtRFM.Text = HttpUtility.HtmlDecode(gvrow.Cells[10].Text);
                lblResult.Visible = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                hfCode.Value = code;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)////buat edit
        {
            int No = Convert.ToInt32(lblCountryCode.Text);
            string category = ddl1edit.SelectedItem.Text;
            string tod = ddl2edit.SelectedItem.Text;
            string cf = ddl3edit.SelectedItem.Text;
            string tabel = ddl4edit.SelectedItem.Text;
            string rfb = txtRFB.Text;
            string rfm = txtRFM.Text;
            string dt = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            executeUpdate(No, category, tod, cf, tabel, rfb, rfm, dt);///masuk ke private void update                  
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Records Updated Successfully');");
            sb.Append("$('#editModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
        }

        private void executeUpdate(int No, string category, string tod, string cf, string tabel, string rfb, string rfm, string dt)///buat edit(klik button update di form edit)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand updateCmd = new SqlCommand("SP_SaveLHKAcquisitionEdit", conn);
                updateCmd.CommandType = CommandType.StoredProcedure;
                updateCmd.Parameters.AddWithValue("@category", category);
                updateCmd.Parameters.AddWithValue("@tod", tod);
                updateCmd.Parameters.AddWithValue("@cf", cf);
                updateCmd.Parameters.AddWithValue("@tabel", tabel);
                updateCmd.Parameters.AddWithValue("@rfb", rfb);
                updateCmd.Parameters.AddWithValue("@rfm", rfm);
                updateCmd.Parameters.AddWithValue("@dt", dt);
                updateCmd.Parameters.AddWithValue("@No", No);
                updateCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException me)
            {
                System.Console.Error.Write(me.InnerException.Data);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)///buat add, menegluarkan form add
        {
            Getddl1();
            txtTotalPopulation.Text = string.Empty;
            txtIndYear.Text = string.Empty;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);
        }

        protected void btnAddRecord_Click(object sender, EventArgs e)///klik buton add di form add
        {
            string IDProc_B = txtIDSaveTemp.Text;
            string IDProc_A = txtIDSave.Text;
            string modul = "LHK_Acquisition";
            string doe = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");

            string Headdb = txbUsername.Text;
            string BAVcbng = txbCabang.Text;
            string datestart = txbPeriodeStart.Text;
            string dateend = txbPeriodeEnd.Text;
            string accID = txtAccountId.Text;
            string nmMitra = txtNamaMitra.Text;
            string cenName = txtCentName.Text;
            string DisbDate = txtDisbDate.Text;
            string par = txtPAR.Text;
            string loanStat = txtLoanStat.Text;
            string nmSO = txtNamaSO.Text;
            string nmRO = txtNamaRO.Text;
            string ukmDate = txtUkmDate.Text;
            string outstand = txtOutstanding.Text;
            string ticSize = txtTicketSize.Text;


            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                try
                {
                    string mod = modul.ToString();
                    int idlhk = Convert.ToInt32(IDProc_A);
                    string code = ddl1.SelectedItem.Text;
                    string name = ddl2.SelectedItem.Text;
                    string region = ddl4.SelectedItem.Text;
                    string continent = ddl3.SelectedItem.Text;
                    string population = txtTotalPopulation.Text;
                    string indyear = txtIndYear.Text;
                    string dt = doe.ToString();

                    string Headdbs = Headdb.ToString();
                    string BAVcbngs = BAVcbng.ToString();
                    string datestarts = datestart.ToString();
                    string dateends = dateend.ToString();
                    string accIDs = accID.ToString();
                    string nmMitras = nmMitra.ToString();
                    string cenNames = cenName.ToString();
                    string DisbDates = DisbDate.ToString();
                    string pars = par.ToString();
                    string loanStats = loanStat.ToString();
                    string nmSOs = nmSO.ToString();
                    string nmROs = nmRO.ToString();
                    string ukmDates = ukmDate.ToString();
                    string outstands = outstand.ToString();
                    string ticSizes = ticSize.ToString();

                    executeAdd(idlhk, code, name, continent, region, population, indyear, mod, doe, Headdbs,BAVcbngs,datestarts,dateends,accIDs,nmMitras,cenNames,DisbDates,pars,loanStats,nmSOs,nmROs,ukmDates,outstands,ticSizes);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }

            }

            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                try
                {
                    //txtidlhk.Text = "12";
                    string mod = modul.ToString();
                    int idlhk = Convert.ToInt32(IDProc_B);
                    string code = ddl1.SelectedItem.Text;
                    string name = ddl2.SelectedItem.Text;
                    string region = ddl4.SelectedItem.Text;
                    string continent = ddl3.SelectedItem.Text;
                    string population = txtTotalPopulation.Text;
                    string indyear = txtIndYear.Text;
                    string dt = doe.ToString();

                    string Headdbs = Headdb.ToString();
                    string BAVcbngs = BAVcbng.ToString();
                    string datestarts = datestart.ToString();
                    string dateends = dateend.ToString();
                    string accIDs = accID.ToString();
                    string nmMitras = nmMitra.ToString();
                    string cenNames = cenName.ToString();
                    string DisbDates = DisbDate.ToString();
                    string pars = par.ToString();
                    string loanStats = loanStat.ToString();
                    string nmSOs = nmSO.ToString();
                    string nmROs = nmRO.ToString();
                    string ukmDates = ukmDate.ToString();
                    string outstands = outstand.ToString();
                    string ticSizes = ticSize.ToString();

                    executeAdd(idlhk, code, name, continent, region, population, indyear, mod, doe, Headdbs, BAVcbngs, datestarts, dateends, accIDs, nmMitras, cenNames, DisbDates, pars, loanStats, nmSOs, nmROs, ukmDates, outstands, ticSizes);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }

            }

        }

        private void executeAdd(int idlhk, string code, string name, string continent, string region, string population, string indyear, string mod, string doe,
            string Headdbs,string BAVcbngs,string datestarts,string dateends,string accIDs,string nmMitras,string cenNames,string DisbDates,string pars,
            string loanStats,string nmSOs,string nmROs,string ukmDates,string outstands,string ticSizes)///Adding data
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand addCmd = new SqlCommand("SP_SaveLHKAcquisition", conn);
                addCmd.CommandType = CommandType.StoredProcedure;
                addCmd.Parameters.AddWithValue("@idlhk", idlhk);
                addCmd.Parameters.AddWithValue("@mod", mod);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.Parameters.AddWithValue("@name", name);
                addCmd.Parameters.AddWithValue("@continent", continent);
                addCmd.Parameters.AddWithValue("@region", region);
                addCmd.Parameters.AddWithValue("@population", population);
                addCmd.Parameters.AddWithValue("@indyear", indyear);
                addCmd.Parameters.AddWithValue("@doe", doe);

                addCmd.Parameters.AddWithValue("@Headdbs", Headdbs);
                addCmd.Parameters.AddWithValue("@BAVcbngs", BAVcbngs);
                addCmd.Parameters.AddWithValue("@datestarts", datestarts);
                addCmd.Parameters.AddWithValue("@dateends", dateends);
                addCmd.Parameters.AddWithValue("@accIDs", accIDs);
                addCmd.Parameters.AddWithValue("@nmMitras", nmMitras);
                addCmd.Parameters.AddWithValue("@cenNames", cenNames);
                addCmd.Parameters.AddWithValue("@DisbDates", DisbDates);
                addCmd.Parameters.AddWithValue("@pars", pars);
                addCmd.Parameters.AddWithValue("@loanStats", loanStats);
                addCmd.Parameters.AddWithValue("@nmSOs", nmSOs);
                addCmd.Parameters.AddWithValue("@nmROs", nmROs);
                addCmd.Parameters.AddWithValue("@ukmDates", ukmDates);
                addCmd.Parameters.AddWithValue("@outstands", outstands);
                addCmd.Parameters.AddWithValue("@ticSizes", ticSizes);


                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string code = hfCode.Value;
            executeDelete(code);
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Record deleted Successfully');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);

        }

        private void executeDelete(string code)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string updatecmd = "delete from LHK_Acquisition where No=@code";
                SqlCommand addCmd = new SqlCommand(updatecmd, conn);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }

        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }

        private void Getddl1()
        {
            ddl1.AppendDataBoundItems = true;
            ddl2.AppendDataBoundItems = true;
            ddl3.AppendDataBoundItems = true;
            ddl4.AppendDataBoundItems = true;

            ddl1.Items.Clear();
            ddl1.Items.Add(new ListItem("--Select category--", ""));
            ddl2.Items.Clear();
            ddl2.Items.Add(new ListItem("--Select Type--", ""));
            ddl3.Items.Clear();
            ddl3.Items.Add(new ListItem("--Select Finding--", ""));
            ddl4.Items.Clear();
            ddl4.Items.Add(new ListItem("--Select Tabel--", ""));


            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select [ID_LHK_Acquisition_Category],[Category_Name] from Master_LHK_Acquisition_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddl1.DataSource = cmd.ExecuteReader();
                ddl1.DataTextField = "Category_Name";
                ddl1.DataValueField = "ID_LHK_Acquisition_Category";
                ddl1.DataBind();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddl2.Items.Clear();
            ddl2.Items.Add(new ListItem("--Select Type--", ""));
            ddl3.Items.Clear();
            ddl3.Items.Add(new ListItem("--Select Finding--", ""));
            ddl4.Items.Clear();
            ddl4.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHK_Acquisition_TOD, TOD_Name from Master_LHK_Acquisition_TOD " +
                               "where ID_LHK_Acquisition_Category=@ID_LHK_Acquisition_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHK_Acquisition_Category", ddl1.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddl2.DataSource = cmd.ExecuteReader();
                ddl2.DataTextField = "TOD_Name";
                ddl2.DataValueField = "ID_LHK_Acquisition_TOD";
                ddl2.DataBind();

                if (ddl2.Items.Count > 1)
                {
                    ddl1.Enabled = true;
                    ddl2.Enabled = true;
                }
                else
                {
                    ddl2.Enabled = false;
                    ddl3.Enabled = false;
                    ddl4.Enabled = false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddl2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddl3.Items.Clear();
            ddl3.Items.Add(new ListItem("--Select Finding--", ""));
            //ddl3.AppendDataBoundItems = true;
            ddl4.Items.Clear();
            ddl4.Items.Add(new ListItem("--Select Tabel--", ""));
            //ddl4.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHK_Acquisition_Finding, Finding_Name from Master_LHK_Acquisition_Finding " +
                                        "where ID_LHK_Acquisition_TOD=@ID_LHK_Acquisition_TOD";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHK_Acquisition_TOD", ddl2.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddl3.DataSource = cmd.ExecuteReader();
                ddl3.DataTextField = "Finding_Name";
                ddl3.DataValueField = "ID_LHK_Acquisition_Finding";
                ddl3.DataBind();

                if (ddl3.Items.Count > 1)
                {
                    ddl3.Enabled = true;
                    //ddl4.Enabled = true;
                }

                else
                {
                    ddl3.Enabled = false;
                    ddl4.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddl3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddl4.Items.Clear();
            ddl4.Items.Add(new ListItem("--Select Tabel--", ""));
            //ddl4.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHK_Acquisition_Tabel, Tabel_Name from Master_LHK_Acquisition_Tabel " +
                                        "where ID_LHK_Acquisition_Finding=@ID_LHK_Acquisition_Finding";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHK_Acquisition_Finding", ddl3.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddl4.DataSource = cmd.ExecuteReader();
                ddl4.DataTextField = "Tabel_Name";
                ddl4.DataValueField = "ID_LHK_Acquisition_Tabel";
                ddl4.DataBind();

                if (ddl4.Items.Count > 1)
                {
                    ddl4.Enabled = true;
                }

                else
                {
                    ddl4.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        private void Getddl1edit()
        {
            ddl1edit.AppendDataBoundItems = true;
            ddl1edit.Items.Clear();
            ddl1edit.Items.Add(new ListItem("--Select category--", ""));
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select [ID_LHK_Acquisition_Category],[Category_Name] from Master_LHK_Acquisition_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddl1edit.DataSource = cmd.ExecuteReader();
                ddl1edit.DataTextField = "Category_Name";
                ddl1edit.DataValueField = "ID_LHK_Acquisition_Category";
                ddl1edit.DataBind();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddl1edit_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddl2edit.Items.Clear();
            ddl2edit.Items.Add(new ListItem("--Select Type--", ""));
            ddl3edit.Items.Clear();
            ddl3edit.Items.Add(new ListItem("--Select Finding--", ""));
            ddl4edit.Items.Clear();
            ddl4edit.Items.Add(new ListItem("--Select Tabel--", ""));

            ddl2edit.AppendDataBoundItems = true;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHK_Acquisition_TOD, TOD_Name from Master_LHK_Acquisition_TOD " +
                               "where ID_LHK_Acquisition_Category=@ID_LHK_Acquisition_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHK_Acquisition_Category", ddl1edit.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddl2edit.DataSource = cmd.ExecuteReader();
                ddl2edit.DataTextField = "TOD_Name";
                ddl2edit.DataValueField = "ID_LHK_Acquisition_TOD";
                ddl2edit.DataBind();

                if (ddl1edit.Items.Count > 1)
                {
                    ddl1edit.Enabled = true;
                }
                else
                {
                    ddl2edit.Enabled = false;
                    ddl3edit.Enabled = false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }  

        }

        protected void ddl2edit_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddl3edit.Items.Clear();
            ddl3edit.Items.Add(new ListItem("--Select Finding--", ""));
            ddl3edit.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHK_Acquisition_Finding, Finding_Name from Master_LHK_Acquisition_Finding " +
                                        "where ID_LHK_Acquisition_TOD=@ID_LHK_Acquisition_TOD";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHK_Acquisition_TOD", ddl2edit.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddl3edit.DataSource = cmd.ExecuteReader();
                ddl3edit.DataTextField = "Finding_Name";
                ddl3edit.DataValueField = "ID_LHK_Acquisition_Finding";
                ddl3edit.DataBind();

                if (ddl3edit.Items.Count > 1)
                {
                    ddl3edit.Enabled = true;
                }

                else
                {
                    ddl3edit.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddl3edit_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddl4edit.Items.Clear();
            ddl4edit.Items.Add(new ListItem("--Select Tabel--", ""));
            ddl4edit.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHK_Acquisition_Tabel, Tabel_Name from Master_LHK_Acquisition_Tabel " +
                                        "where ID_LHK_Acquisition_Finding=@ID_LHK_Acquisition_Finding";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHK_Acquisition_Finding", ddl3edit.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddl4edit.DataSource = cmd.ExecuteReader();
                ddl4edit.DataTextField = "Tabel_Name";
                ddl4edit.DataValueField = "ID_LHK_Acquisition_Tabel";
                ddl4edit.DataBind();

                if (ddl4edit.Items.Count > 1)
                {
                    ddl4edit.Enabled = true;
                }

                else
                {
                    ddl4edit.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }


        
        public void BindGridColl()
        {
            string IDProc_B = txtIDSaveTemp.Text;
            string IDProc_A = txtIDSave.Text;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_Collection where ID_LHK={0}", IDProc_A);
                    SqlConnection con = new SqlConnection(cnString);
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    GridView2.DataSource = dt;
                    GridView2.DataBind();

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }

            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_Collection where ID_LHK={0}", IDProc_B);
                    SqlConnection con = new SqlConnection(cnString);
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    GridView2.DataSource = dt;
                    GridView2.DataBind();

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }
            }
        }

        protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                IEnumerable<DataRow> query = from i in dt.AsEnumerable()
                                             where i.Field<Int32>("No").Equals(code)
                                             select i;
                DataTable detailTable = query.CopyToDataTable<DataRow>();
                DetailsView1.DataSource = detailTable;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("editRecord"))///buat edit
            {
                Getddl1Coledit();
                GridViewRow gvrow = GridView2.Rows[index];
                Label1.Text = HttpUtility.HtmlDecode(gvrow.Cells[3].Text).ToString();
                ddledit1coll.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[5].Text);
                ddledit2coll.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[6].Text);
                ddledit3coll.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[7].Text);
                ddledit4coll.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[8].Text);
                txbEditCollRFB.Text = HttpUtility.HtmlDecode(gvrow.Cells[9].Text);
                txbEditCollRFM.Text = HttpUtility.HtmlDecode(gvrow.Cells[10].Text);
                Label2.Visible = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#Div2Edit').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);
                //ClientScript.RegisterStartupScript(GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string code = GridView2.DataKeys[index].Value.ToString();
                hfCode.Value = code;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#Div4Del').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }

        }

        protected void BtnSaveColl_Click(object sender, EventArgs e)
        {
            int No = Convert.ToInt32(Label1.Text);
            string category = ddledit1coll.SelectedItem.Text;
            string tod = ddledit2coll.SelectedItem.Text;
            string cf = ddledit3coll.SelectedItem.Text;
            string tabel = ddledit4coll.SelectedItem.Text;
            string rfb = txbEditCollRFB.Text;
            string rfm = txbEditCollRFM.Text;
            string dt = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            executeUpdateCol(No, category, tod, cf, tabel, rfb, rfm, dt);///masuk ke private void update                  
            BindGridColl();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Records Updated Successfully');");
            sb.Append("$('#Div2Edit').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);

        }

        private void executeUpdateCol(int No, string category, string tod, string cf, string tabel, string rfb, string rfm, string dt)///buat edit(klik button update di form edit)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand updateCmd = new SqlCommand("SP_SaveLHKCollectionEdit", conn);
                updateCmd.CommandType = CommandType.StoredProcedure;
                updateCmd.Parameters.AddWithValue("@category", category);
                updateCmd.Parameters.AddWithValue("@tod", tod);
                updateCmd.Parameters.AddWithValue("@cf", cf);
                updateCmd.Parameters.AddWithValue("@tabel", tabel);
                updateCmd.Parameters.AddWithValue("@rfb", rfb);
                updateCmd.Parameters.AddWithValue("@rfm", rfm);
                updateCmd.Parameters.AddWithValue("@dt", dt);
                updateCmd.Parameters.AddWithValue("@No", No);
                updateCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException me)
            {
                System.Console.Error.Write(me.InnerException.Data);
            }
        }

        protected void btnAddCollection_Click(object sender, EventArgs e)///buat add, menegluarkan form ad
        {
            Getddl1Col();
            txbAddCollRFB.Text = string.Empty;
            txbAddCollRFM.Text = string.Empty;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#Div3Add').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);

        }

        protected void btnAddColl_Click(object sender, EventArgs e)///klik buton add di form add
        {
            string IDProc_B = txtIDSaveTemp.Text;
            string IDProc_A = txtIDSave.Text;
            string modul = "LHK_Collection";
            string doe = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");

            string Headdb = txbUsername.Text;
            string BAVcbng = txbCabang.Text;
            string datestart = txbPeriodeStart.Text;
            string dateend = txbPeriodeEnd.Text;
            string accID = txtAccountId.Text;
            string nmMitra = txtNamaMitra.Text;
            string cenName = txtCentName.Text;
            string DisbDate = txtDisbDate.Text;
            string par = txtPAR.Text;
            string loanStat = txtLoanStat.Text;
            string nmSO = txtNamaSO.Text;
            string nmRO = txtNamaRO.Text;
            string ukmDate = txtUkmDate.Text;
            string outstand = txtOutstanding.Text;
            string ticSize = txtTicketSize.Text;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                try
                {
                    //txtidlhk.Text = "12";
                    string mod = modul.ToString();
                    int idlhk = Convert.ToInt32(IDProc_A);
                    string code = ddladd1coll.SelectedItem.Text;
                    string name = ddladd2coll.SelectedItem.Text;
                    string region = ddladd4coll.SelectedItem.Text;
                    string continent = ddladd3coll.SelectedItem.Text;
                    string population = txbAddCollRFB.Text;
                    string indyear = txbAddCollRFM.Text;
                    string dt = doe.ToString();

                    string Headdbs = Headdb.ToString();
                    string BAVcbngs = BAVcbng.ToString();
                    string datestarts = datestart.ToString();
                    string dateends = dateend.ToString();
                    string accIDs = accID.ToString();
                    string nmMitras = nmMitra.ToString();
                    string cenNames = cenName.ToString();
                    string DisbDates = DisbDate.ToString();
                    string pars = par.ToString();
                    string loanStats = loanStat.ToString();
                    string nmSOs = nmSO.ToString();
                    string nmROs = nmRO.ToString();
                    string ukmDates = ukmDate.ToString();
                    string outstands = outstand.ToString();
                    string ticSizes = ticSize.ToString();

                    executeAddCol(idlhk, code, name, continent, region, population, indyear, mod, doe, Headdbs, BAVcbngs, datestarts, dateends, accIDs, nmMitras, cenNames, DisbDates, pars, loanStats, nmSOs, nmROs, ukmDates, outstands, ticSizes);
                    BindGridColl();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#Div3Add').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }

            }

            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                try
                {
                    //txtidlhk.Text = "12";
                    string mod = modul.ToString();
                    int idlhk = Convert.ToInt32(IDProc_B);
                    string code = ddladd1coll.SelectedItem.Text;
                    string name = ddladd2coll.SelectedItem.Text;
                    string region = ddladd4coll.SelectedItem.Text;
                    string continent = ddladd3coll.SelectedItem.Text;
                    string population = txbAddCollRFB.Text;
                    string indyear = txbAddCollRFM.Text;
                    string dt = doe.ToString();

                    string Headdbs = Headdb.ToString();
                    string BAVcbngs = BAVcbng.ToString();
                    string datestarts = datestart.ToString();
                    string dateends = dateend.ToString();
                    string accIDs = accID.ToString();
                    string nmMitras = nmMitra.ToString();
                    string cenNames = cenName.ToString();
                    string DisbDates = DisbDate.ToString();
                    string pars = par.ToString();
                    string loanStats = loanStat.ToString();
                    string nmSOs = nmSO.ToString();
                    string nmROs = nmRO.ToString();
                    string ukmDates = ukmDate.ToString();
                    string outstands = outstand.ToString();
                    string ticSizes = ticSize.ToString();

                    executeAddCol(idlhk, code, name, continent, region, population, indyear, mod, doe, Headdbs, BAVcbngs, datestarts, dateends, accIDs, nmMitras, cenNames, DisbDates, pars, loanStats, nmSOs, nmROs, ukmDates, outstands, ticSizes);
                    BindGridColl();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#Div3Add').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }

            }

        }

        private void executeAddCol(int idlhk, string code, string name, string continent, string region, string population, string indyear, string mod, string doe,
            string Headdbs, string BAVcbngs, string datestarts, string dateends, string accIDs, string nmMitras, string cenNames, string DisbDates, string pars, string loanStats,
            string nmSOs, string nmROs, string ukmDates, string outstands, string ticSizes)///Adding data
        {
            //txtidlhk.Text = "12";
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand addCmd = new SqlCommand("SP_SaveLHKCollection", conn);
                addCmd.CommandType = CommandType.StoredProcedure;
                addCmd.Parameters.AddWithValue("@idlhk", idlhk);
                addCmd.Parameters.AddWithValue("@mod", mod);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.Parameters.AddWithValue("@name", name);
                addCmd.Parameters.AddWithValue("@continent", continent);
                addCmd.Parameters.AddWithValue("@region", region);
                addCmd.Parameters.AddWithValue("@population", population);
                addCmd.Parameters.AddWithValue("@indyear", indyear);
                addCmd.Parameters.AddWithValue("@doe", doe);

                addCmd.Parameters.AddWithValue("@Headdbs", Headdbs);
                addCmd.Parameters.AddWithValue("@BAVcbngs", BAVcbngs);
                addCmd.Parameters.AddWithValue("@datestarts", datestarts);
                addCmd.Parameters.AddWithValue("@dateends", dateends);
                addCmd.Parameters.AddWithValue("@accIDs", accIDs);
                addCmd.Parameters.AddWithValue("@nmMitras", nmMitras);
                addCmd.Parameters.AddWithValue("@cenNames", cenNames);
                addCmd.Parameters.AddWithValue("@DisbDates", DisbDates);
                addCmd.Parameters.AddWithValue("@pars", pars);
                addCmd.Parameters.AddWithValue("@loanStats", loanStats);
                addCmd.Parameters.AddWithValue("@nmSOs", nmSOs);
                addCmd.Parameters.AddWithValue("@nmROs", nmROs);
                addCmd.Parameters.AddWithValue("@ukmDates", ukmDates);
                addCmd.Parameters.AddWithValue("@outstands", outstands);
                addCmd.Parameters.AddWithValue("@ticSizes", ticSizes);

                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }
        }

        protected void btnDelCol_Click(object sender, EventArgs e)
        {
            string code = hfCode.Value;
            executeDeleteCol(code);
            BindGridColl();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Record deleted Successfully');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);

        }

        private void executeDeleteCol(string code)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string updatecmd = "delete from LHK_Collection where No=@code";
                SqlCommand addCmd = new SqlCommand(updatecmd, conn);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }

        }

        protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView2.PageIndex = e.NewPageIndex;
            GridView2.DataBind();
        }

        private void Getddl1Col()
        {
            ddladd1coll.AppendDataBoundItems = true;
            ddladd2coll.AppendDataBoundItems = true;
            ddladd3coll.AppendDataBoundItems = true;
            ddladd4coll.AppendDataBoundItems = true;

            ddladd1coll.Items.Clear();
            ddladd1coll.Items.Add(new ListItem("--Select category--", ""));
            ddladd2coll.Items.Clear();
            ddladd2coll.Items.Add(new ListItem("--Select Type--", ""));
            ddladd3coll.Items.Clear();
            ddladd3coll.Items.Add(new ListItem("--Select Finding--", ""));
            ddladd4coll.Items.Clear();
            ddladd4coll.Items.Add(new ListItem("--Select Tabel--", ""));


            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select [ID_LHK_Collection_Category],[Category_Name] from Master_LHK_Collection_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddladd1coll.DataSource = cmd.ExecuteReader();
                ddladd1coll.DataTextField = "Category_Name";
                ddladd1coll.DataValueField = "ID_LHK_Collection_Category";
                ddladd1coll.DataBind();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddladd1coll_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddladd2coll.Items.Clear();
            ddladd2coll.Items.Add(new ListItem("--Select Type--", ""));
            ddladd3coll.Items.Clear();
            ddladd3coll.Items.Add(new ListItem("--Select Finding--", ""));
            ddladd4coll.Items.Clear();
            ddladd4coll.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHK_Collection_TOD, TOD_Name from Master_LHK_Collection_TOD " +
                               "where ID_LHK_Collection_Category=@ID_LHK_Collection_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHK_Collection_Category", ddladd1coll.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddladd2coll.DataSource = cmd.ExecuteReader();
                ddladd2coll.DataTextField = "TOD_Name";
                ddladd2coll.DataValueField = "ID_LHK_Collection_TOD";
                ddladd2coll.DataBind();

                if (ddladd2coll.Items.Count > 1)
                {
                    ddladd1coll.Enabled = true;
                    ddladd2coll.Enabled = true;
                }
                else
                {
                    ddladd2coll.Enabled = false;
                    ddladd3coll.Enabled = false;
                    ddladd4coll.Enabled = false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddladd2coll_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddladd3coll.Items.Clear();
            ddladd3coll.Items.Add(new ListItem("--Select Finding--", ""));
            ddladd4coll.Items.Clear();
            ddladd4coll.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHK_Collection_Finding, Finding_Name from Master_LHK_Collection_Finding " +
                                        "where ID_LHK_Collection_TOD=@ID_LHK_Collection_TOD";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHK_Collection_TOD", ddladd2coll.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddladd3coll.DataSource = cmd.ExecuteReader();
                ddladd3coll.DataTextField = "Finding_Name";
                ddladd3coll.DataValueField = "ID_LHK_Collection_Finding";
                ddladd3coll.DataBind();

                if (ddladd3coll.Items.Count > 1)
                {
                    ddladd3coll.Enabled = true;
                    //ddl4.Enabled = true;
                }

                else
                {
                    ddladd3coll.Enabled = false;
                    ddladd4coll.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddladd3coll_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddladd4coll.Items.Clear();
            ddladd4coll.Items.Add(new ListItem("--Select Tabel--", ""));
            //ddl4.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHK_Collection_Tabel, Tabel_Name from Master_LHK_Collection_Tabel " +
                                        "where ID_LHK_Collection_Finding=@ID_LHK_Collection_Finding";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHK_Collection_Finding", ddladd3coll.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddladd4coll.DataSource = cmd.ExecuteReader();
                ddladd4coll.DataTextField = "Tabel_Name";
                ddladd4coll.DataValueField = "ID_LHK_Collection_Tabel";
                ddladd4coll.DataBind();

                if (ddladd4coll.Items.Count > 1)
                {
                    ddladd4coll.Enabled = true;
                }

                else
                {
                    ddladd4coll.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        private void Getddl1Coledit()
        {
            ddledit1coll.AppendDataBoundItems = true;
            ddledit1coll.Items.Clear();
            ddledit1coll.Items.Add(new ListItem("--Select category--", ""));
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select [ID_LHK_Collection_Category],[Category_Name] from Master_LHK_Collection_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddledit1coll.DataSource = cmd.ExecuteReader();
                ddledit1coll.DataTextField = "Category_Name";
                ddledit1coll.DataValueField = "ID_LHK_Collection_Category";
                ddledit1coll.DataBind();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddledit1coll_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddledit2coll.Items.Clear();
            ddledit2coll.Items.Add(new ListItem("--Select Type--", ""));
            ddledit3coll.Items.Clear();
            ddledit3coll.Items.Add(new ListItem("--Select Finding--", ""));
            ddledit4coll.Items.Clear();
            ddledit4coll.Items.Add(new ListItem("--Select Tabel--", ""));

            ddledit2coll.AppendDataBoundItems = true;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHK_Collection_TOD, TOD_Name from Master_LHK_Collection_TOD " +
                               "where ID_LHK_Collection_Category=@ID_LHK_Collection_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHK_Collection_Category", ddledit1coll.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddledit2coll.DataSource = cmd.ExecuteReader();
                ddledit2coll.DataTextField = "TOD_Name";
                ddledit2coll.DataValueField = "ID_LHK_Acquisition_TOD";
                ddledit2coll.DataBind();

                if (ddledit1coll.Items.Count > 1)
                {
                    ddledit1coll.Enabled = true;
                }
                else
                {
                    ddledit2coll.Enabled = false;
                    ddledit3coll.Enabled = false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddledit2coll_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddledit3coll.Items.Clear();
            ddledit3coll.Items.Add(new ListItem("--Select Finding--", ""));
            ddledit3coll.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHK_Collection_Finding, Finding_Name from Master_LHK_Collection_Finding " +
                                        "where ID_LHK_Collection_TOD=@ID_LHK_Collection_TOD";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHK_Collection_TOD", ddledit2coll.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddledit3coll.DataSource = cmd.ExecuteReader();
                ddledit3coll.DataTextField = "Finding_Name";
                ddledit3coll.DataValueField = "ID_LHK_Collection_Finding";
                ddledit3coll.DataBind();

                if (ddledit3coll.Items.Count > 1)
                {
                    ddledit3coll.Enabled = true;
                }

                else
                {
                    ddledit3coll.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddledit3coll_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddledit4coll.Items.Clear();
            ddledit4coll.Items.Add(new ListItem("--Select Tabel--", ""));
            ddledit4coll.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHK_Collection_Tabel, Tabel_Name from Master_LHK_Collection_Tabel " +
                                        "where ID_LHK_Collection_Finding=@ID_LHK_Collection_Finding";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHK_Collection_Finding", ddledit3coll.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddledit4coll.DataSource = cmd.ExecuteReader();
                ddledit4coll.DataTextField = "Tabel_Name";
                ddledit4coll.DataValueField = "ID_LHK_Collection_Tabel";
                ddledit4coll.DataBind();

                if (ddledit4coll.Items.Count > 1)
                {
                    ddledit4coll.Enabled = true;
                }

                else
                {
                    ddledit4coll.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

    }
}