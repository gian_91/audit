﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Data;
using System.Web.Services;
using System.Data.SqlClient;

namespace Audit.Class
{
    public  class Extention
    {
        [WebMethod]
        public  List<string> GetEmpNames(string empName)
        {
            List<string> Emp = new List<string>();
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            string query = string.Format("select ClientID from RawDataCurrent where ClientID LIKE '{0}%'", empName);
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Emp.Add(reader.GetString(0));
                    }
                }
            }
            return Emp;
        }  

    }
}