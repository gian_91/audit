﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;
using System.Drawing;
using System.Configuration;
using System.Data.SqlClient;
using Audit.DAL2;

using System.Net.Security;
using System.Security.Cryptography.X509Certificates;


namespace Audit
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
        DaLayer dl = new DaLayer();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void registration_click(object sender, EventArgs e)
        {
            string username = string.Empty;
            string password = string.Empty;
            //string userid = string.Empty;
            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT UserId,Username, [Password] FROM tlbuser WHERE Email = @Email and NIK = @NIK"))
                {
                    cmd.Parameters.AddWithValue("@Email", Email.Value.Trim());
                    cmd.Parameters.AddWithValue("@NIK", txtNik.Value.Trim());
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.Read())
                        {
                            //userid = sdr["UserId"].ToString();
                            username = sdr["Username"].ToString();
                            password = sdr["Password"].ToString();
                            password = dl.Decrypt(password);

                        }
                    }
                    con.Close();
                }
            }
            try
            {
                if (!string.IsNullOrEmpty(password))
                {
                    MailMessage mm = new MailMessage("bavsupport@bina-artha.net", Email.Value.Trim());
                    mm.Subject = "Password Recovery";
                    mm.Body = string.Format("Hi {0},<br /><br />Your password is {1}.<br /><br /><br />Thank You.", username, password);
                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.merahputih.id";
                    smtp.EnableSsl = true;

                    NetworkCredential NetworkCred = new NetworkCredential();
                    NetworkCred.UserName = "bavsupport@merahputih.id";
                    NetworkCred.Password = "init1234";

                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate(object s,
                    System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                    System.Security.Cryptography.X509Certificates.X509Chain chain,
                    System.Net.Security.SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    };
                    smtp.Send(mm);
                    lblMessage.ForeColor = Color.White;
                    lblMessage.Text = "Password has been sent to your email address.";
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "redirectJS", "setTimeout(function() { window.location.replace('Login.aspx') }, 4000);", true);
                }
                else
                {
                    lblMessage.ForeColor = Color.White;
                    lblMessage.Text = "This email address or NIK does not match our records.";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);

                if (ex.InnerException != null)
                {
                    Console.WriteLine("InnerException is: {0}", ex.InnerException);
                }

            }
 
        }

        protected void Submit1_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }

        //private void executeUpdate(int No, string category, string tod, string cf, string tabel, string rfb, string rfm)//buat edit(klik button update di form edit)
        //{
        //    string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
        //    try
        //    {
        //        SqlConnection conn = new SqlConnection(connString);
        //        conn.Open();
        //        string updatecmd = "update LHP_Acquisition set LHP_Acquisition_Category = @category ,LHP_Acquisition_TOD= @tod ,LHP_Acquisition_CaseFinding= @cf ,LHP_Acquisition_Tabel=@tabel," +
        //            "LHP_Acquisition_Recomd_Branch=@rfb ,LHP_Acquisition_Recomd_Management=@rfm where [No]= @No";
        //        SqlCommand updateCmd = new SqlCommand(updatecmd, conn);
        //        updateCmd.Parameters.AddWithValue("@category", category);
        //        updateCmd.Parameters.AddWithValue("@tod", tod);
        //        updateCmd.Parameters.AddWithValue("@cf", cf);
        //        updateCmd.Parameters.AddWithValue("@tabel", tabel);
        //        updateCmd.Parameters.AddWithValue("@rfb", rfb);
        //        updateCmd.Parameters.AddWithValue("@rfm", rfm);
        //        updateCmd.Parameters.AddWithValue("@No", No);
        //        updateCmd.ExecuteNonQuery();
        //        conn.Close();
        //    }
        //    catch (SqlException me)
        //    {
        //        System.Console.Error.Write(me.InnerException.Data);
        //    }
        //}

    }
}