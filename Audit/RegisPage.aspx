﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegisPage.aspx.cs" Inherits="Audit.RegisPage" %>
<!DOCTYPE html>
<script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
<link href="Scripts/Bootstrap/bootstrap.min.css" rel="stylesheet" media="screen" />
<script type="text/javascript">
    window.onload = function () {
        var txtPassword = document.getElementById("txtPassword");
        var txtConfirmPassword = document.getElementById("txtConfirmPassword");
        txtPassword.onchange = ConfirmPassword
        txtConfirmPassword.onkeyup = ConfirmPassword
        function ConfirmPassword() {
            txtConfirmPassword.setCustomValidity("");
            if (txtPassword.value != txtConfirmPassword.value) {
                txtConfirmPassword.setCustomValidity("Passwords do not match.");
            }
        }
    }
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
            <style type="text/css">
            body {
                font-family: Arial;
                font-size: 10pt;
                background: url("Images/background_full.jpg") fixed;
                background-size:cover;

            }
        input[type=text], input[type=password]
        {
            width: 200px;
        }   
        table
        {
            background: #F7F7F7;
            border: 1px solid #ccc;
        }
        table th
        {
            background-color: #f00;
            color: #f00;
            font-weight: bold;
        }
        table th, table td
        {
            padding: 5px;
            color: #414751;
        }

        .posisi {
            width:400px;
            margin-left:auto;
            margin-right:auto;
            margin-top:150px;


        }
    </style>
</head>
<body>
    <div align="center">
        <form method="post" action="RegisPage.aspx" id="form2" runat="server">
            <div style="max-width: 400px;">
                <h2 class="form-signin-heading" style="color: #FFFFFF">Registration</h2>
                <br />
                <label for="txtUsername" style="color: #FFFFFF">
                    Username</label>
                <input name="txtUsername" type="text" id="txtUsername" class="form-control" placeholder="Enter Username" runat="server"
                    required />
                <br />
                <label for="txtNik" style="color: #FFFFFF">
                    NIK</label>
                <input name="txtNik" type="text" id="txtNik" class="form-control" placeholder="Enter NIK" runat="server"
                    required />
                <br />
                <label for="txtPassword" style="color: #FFFFFF">
                    Password</label>
                <input name="txtPassword" type="password" id="txtPassword" title="Password must contain: Minimum 8 characters atleast 1 Alphabet and 1 Number" runat="server"
                    class="form-control" placeholder="Enter Password" required pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" />
                <br />
                <label for="txtConfirmPassword" style="color: #FFFFFF">
                    Confirm Password</label>
                <input name="txtConfirmPassword" type="password" id="txtConfirmPassword" class="form-control" runat="server"
                    placeholder="Confirm Password" />
                <br />
                <input type="submit" name="btnSignup" value="Sign up" id="btnSignup" class="btn btn-primary" onserverclick="registration_click" runat="server" />
            </div>
        </form>
    </div>

</body>
</html>
